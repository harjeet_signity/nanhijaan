=== ElasticEmail Plugin ===
Contributors: esideout
Donate link:http://massob.org/donate/
Contact link: http://esideout.com/
Tags: mail, smtp, https, wp_mail, mailer, phpmailer, elasticemail
Requires at least: 2.7
Tested up to: 3.2
Stable tag: 0.9.1

Reconfigures the wp_mail() function to send email using HTTPS (via Elasticemail) instead of SMTP and creates an options page to manage the settings.

== Description ==

The problem: SMTP email delivery by some hosting providers can be extremely slow.  This plugin reconfigures the wp_mail() function to send email using HTTPS (via Elasticemail API) instead of SMTP and creates an options page that allows you to specify various options.

**Plugin Settings Options:**

* Enable or Disable Plugin.
* Elasticemail API username
* Elasticemail assigned API Key
* Elasticemail REST API URL
* Your account channel variable(optional)


== Installation ==

1. Download
2. Upload to your `/wp-contents/plugins/` directory.
3. Activate the plugin through the 'Plugins' menu in WordPress.
4. Go to Elastic Mail Settings by going to the Dashboard, then Settings and clicking on Elastic email API
5. Click "Save Settings"

== Frequently Asked Questions ==

= Will this plugin work with WordPress versions less than 2.7? =

No. WordPress 2.7 changed the way options were updated, so the options page will only work on 2.7 or later.

= Can you add feature x, y or z to the plugin? =

Short answer: maybe.

By all means please contact me to discuss features or options you'd like to see added to the plugin. I can't guarantee to add all of them, but I will consider all sensible requests. I can be contacted here:
<http://esideout.com/contact/>

== Screenshots ==

1. Screenshot of the Options > Elastic Mail Settings.

== Changelog ==

= 0.1 =
* Initial approach, copying the wp_mail function and replacing it

== Upgrade Notice ==
None