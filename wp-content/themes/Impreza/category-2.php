<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );
/**
 * The template for displaying archive pages
 */
$us_layout = US_Layout::instance();

//// Getting current category object

$category = get_category( get_query_var( 'cat' ) );

// Needed for canvas class
$us_layout->titlebar = ( us_get_option( 'titlebar_content', 'all' ) == 'hide' ) ? 'none' : 'default' ;

get_header();

$template_vars = array(
	'layout_type' => us_get_option( 'archive_layout', 'large' ),
	'metas' => (array) us_get_option( 'archive_meta', array() ),
	'content_type' => us_get_option( 'archive_content_type', 'excerpt' ),
	'show_read_more' => in_array( 'read_more', us_get_option( 'archive_meta', array() ) ),
	'pagination' => us_get_option( 'archive_pagination', 'regular' ),
);

$paged 				=	(get_query_var('paged')) ? get_query_var('paged') : 1	;					
$posts_per_page 	= get_option('posts_per_page');

$news_array = array(
	'posts_per_page' 	=> $posts_per_page,
	'cat' 				=> 2, /// News Category
	'order'   			=> 'DESC',
	'paged'				=> $paged
);
$news = query_posts( $news_array );
$total_fetched_news	=	count($news);


?>
<!-- MAIN -->
<!------------------------------------------ Header Image section -------------------------------------------------->
<?php
		$header_img_url	=	wp_get_attachment_url( get_post_thumbnail_id(425) );
?>
	
<section class="l-section wpb_row height_medium imgsize_cover with_img about-header vc_row-fluid">
	<div style="background-image:url(<?php echo $header_img_url ;?>)" class="l-section-img"></div>
	<div class="l-section-h g-html i-cf">
		<div class="g-cols offset_small">
			<div class=" full-width">
				<h1 class="vc_custom_heading" style="font-size: 53px;color: #ffffff;text-align: left;font-family:Lato;font-weight:300;font-style:normal"><?php echo $category->name ; ?></h1>
			</div>
		</div>
	</div>					
</section>

<!------------------------------------------ Header Image section Ends-------------------------------------------------->
<div class="news-page-outer ">
<?php if(!empty($news)) { ?>
<div class="container">
	<div class="row news-page-wrap singlecat">
		<ul>
	<?php foreach($news as $single_news) { 
		setup_postdata($single_news);
		$post_ID = $single_news->ID;
		$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post_ID) );
	?>
			<div class="media single_news_media">
				<div class="media-left">
				<?php if (!empty($feat_image))  { ?>
					<img width="300" height="250" class="img-responsive" src="<?php echo $feat_image ; ?>" />
				<?php } ?>
 				</div>
				<div class="media-body">
					<h1 class="news_title"><?php echo $single_news->post_title ; ?> </h1>
					<p class="news_date"><?php echo date("jS F, Y", strtotime($single_news->post_date)); ?></p>
					<div class="news_content">
					<?php 
						/*
						$pos=strpos($single_news->post_content, '<!--more-->');
						if($pos != 0){
							$splitted_content	=	substr($single_news->post_content,0,$pos);
							echo apply_filters('the_content', $splitted_content);							
						} else {
							echo the_content();
						}
						*/
						if(strlen($single_news->post_content) > 400){
							echo strip_tags(substr($single_news->post_content,0,400));
							?>
								<a href="<?php echo get_permalink($single_news); ?>">... Read More</a>
							<?php
						}else {
							echo strip_tags($single_news->post_content);
						}	
					?>
					</div>
				</div>
			</div>
	<?php
	
	 } 
	 wp_reset_postdata();
	 ?>
		</ul>
	</div>
	<div class="row">
		<div class="navigationgernator_class"><?php wp_pagenavi(); ?></div>
	</div>
</div>
<?php } ?>
</div>
<?php
get_footer();
?>
<style>
	.breadchrub{
		display : none !important;
	}
	.breadcrumbsnew {
		display : block !important;
	}
</style>
