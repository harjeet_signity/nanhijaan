<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );

/**
 * Include all the needed files
 *
 * (!) Note for Clients: please, do not modify this or other theme's files. Use child theme instead!
 */

$us_theme_supports = array(
	'plugins' => array(
		'js_composer' => '/framework/plugins-support/js_composer/js_composer.php',
		'Ultimate_VC_Addons' => '/framework/plugins-support/Ultimate_VC_Addons.php',
		'revslider' => '/framework/plugins-support/revslider.php',
		'contact-form-7' => NULL,
		'gravityforms' => '/framework/plugins-support/gravityforms.php',
		'woocommerce' => '/framework/plugins-support/woocommerce/woocommerce.php',
		'wpml' => NULL,
		'bbpress' => '/framework/plugins-support/bbpress.php',
	),
);

require dirname( __FILE__ ) . '/framework/framework.php';
// Adding css 
function wptuts_scripts_basic()
{
    // For either a plugin or a theme, you can then enqueue the script:
	  wp_enqueue_style( 'custom', get_template_directory_uri() . '/css/custom.css');    
    wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.css');	
  
}
add_action( 'wp_enqueue_scripts', 'wptuts_scripts_basic' );
unset( $us_theme_supports );
/// Creating shortcode for latest feeds (Harjeet Singh) ///
function latest_feed_fun() { ?>
	<div class="latest row" id="latest_news">
		
		<?php
		
			$i	 = 1;
			$new = new WP_Query('post_type=gallery&showposts=4&orderby=date');
			while ($new->have_posts()) : $new->the_post();
				$class_alt	=	($i%2==1) ? "odd" : "even";
		?>
			<div class="col-lg-3 col-sm-6 col-xs-12">	
				
				<div class="single_latest">					
					<?php
						if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
								$img_url	=	wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
							
						} 
						else 
						{
								$img_url	=	home_url().'/wp-content/uploads/2015/07/no_image.jpeg';
						}
					?>
					<div class="single_image">
						<img class="img-responsive" height="300px" src="<?php echo $img_url;?>" >
					</div>
				
					<div class="title_description_<?php echo $class_alt;?>">
						<a class="title-head" href="<?php echo home_url(); ?>/nanhijaan/?page_id=593"><?php 
																	if(strlen($new->post->post_title) > 40 ){
																		echo substr($new->post->post_title,0,40).'...';
																	} else {
																		echo $new->post->post_title;
																	}																	
																?>
						</a>
						<p>
							<?php 
																	if(strlen($new->post->post_content) > 100 ){
																		echo substr($new->post->post_content,0,100).'...';
																	} else {
																		echo $new->post->post_content;
																	}																	
																?>
							<br/>
						</p>
<a href="<?php echo home_url(); ?>/nanhijaan/?page_id=593"class="hubreadmore">Read More -></a>
							<br/>
					</div>
					
					
				</div>
			</div>

		<?php 
		$i++;
		endwhile;

		?>

	</div>


	<?php
}


add_shortcode('latest_feed_short','latest_feed_fun');

function latest_new_fun() { 


	$args = array(
		'cat'      => 2,
		'posts_per_page'     =>1,
		'order'    => 'DESC'
	);
	// The Query
	query_posts( $args );

	// The Loop
	while ( have_posts() ) : the_post();
		$postid	=	get_the_ID();
		$content_post = get_post($postid);
		
		  ?>

		<div class="w-image  align_center">
			<?php
			if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
			  the_post_thumbnail('medium');
			}
			?>
		</div>
		<div class="w-actionbox color_custom controls_bottom" style="background-color:#f8f8f8;color:#666666;">
			<div class="w-actionbox-text">
			<?php	
			if(strlen($content_post->post_content) > 210 ){
			echo substr($content_post->post_content,0,210).'...';
			} else {
			echo $content_post->post_content;
			}
			?>
			</div>
			<div class="w-actionbox-controls">
				<a class="w-btn color_lime style_solid size_medium icon_none" href="<?php echo $content_post->guid ;?>" title="Read More"><span class="w-btn-label">Read More</span></a> 
			</div>
		</div>

		<?php

	endwhile;

	// Reset Query
	wp_reset_query();

}

add_shortcode('latest_new_section','latest_new_fun');


//============================== add custom field on post type ==============================//

   $args1 = array(
				'register_meta_box_cb' => 'add_post_metaboxes_store'
              );

	register_post_type('page', $args1 );

	function add_post_metaboxes_store(){
		add_meta_box('wpt_post_store', 'Mobile Gallery', 'wpt_post_store', 'page', 'normal', 'high');
	}

	function wpt_post_store() {
		global $post;
		global $wpdb;
		if($post->ID == '593') {
			$data			= 	get_post_meta($post->ID, 'speaker_meta_data');
			
			$gallery_arr 	=	$wpdb->get_results( "SELECT * FROM  wp_ngg_gallery", ARRAY_A );
			
			?>
			<div class="speaker-custom-fields-container">
				<div class="time-child-row">
					<label><strong>Select Gallery</strong></label>
					<?php
					    $gallery_dropdown  = "";
						foreach($gallery_arr as $key=>$gallery_rec):
							$selected = '';	
							if(!empty($data)):	
							$selected = (in_array($gallery_rec['gid'], $data[0]))? "selected='selected'" : "";
							endif;				
				$gallery_dropdown  .=  "<option value='".$gallery_rec['gid']."' ".$selected.">".$gallery_rec['title']."</option>";
						endforeach;
					 ?>
					 <select name="speaker_meta_data[]" multiple>
						<?php echo $gallery_dropdown ; ?>
					 </select>
				
				</div>
				
			</div>
		<?php 
		}	
		
	}



function save_post_callback( $post_id ) {
	
	
	if ($_POST['post_type'] == 'page' && $_POST['original_post_status'] == 'auto-draft') {
		//wp_insert_term( $_POST['post_title'], 'topic_categories', $args = array() );
	}
	else if($_POST['post_type'] == 'page') {
		
		update_post_meta($_POST['post_ID'],'speaker_meta_data',$_POST['speaker_meta_data']);
	}
}
add_action( 'save_post', 'save_post_callback' );


/*ONLINE PAYMENT WEBFORM BY SANDEEP*/
function online_donate_payment($atts) {

if(empty($atts['purpose'])){
	$purpose = 'Donation For Nanhijaan';
} else {
	$purpose = $atts['purpose'];
}
$action = home_url().'/make-a-payment/';
echo '<div class=""><div class=" full-width online_donation_form"><div>
<form class="" method="POST" action="'.$action.'">

<div class="">

<div class="row">
<div class="col-md-6"><span class=""><input type="text" placeholder="Your Name*"  class="" size="40" value="" name="payer_name" required="required"></span></div>
<div class="col-md-6"><span class="wpcf7-form-control-wrap mobile-number">
<input type="text" placeholder="Mobile Number*"  required="required"  class="" maxlength="10" size="40" value="" name="payer_mobile"></span>
</div>
</div>


<div class="row">
<div class="col-md-12"><span class="wpcf7-form-control-wrap email"><input type="email" placeholder="Email*"  required="required" class="" size="40" value="" name="payer_email"></span>
</div>
</div>

<div class="row">
<div class="col-md-12"><span class="wpcf7-form-control-wrap address"><textarea placeholder="Address*"  class="" rows="10" cols="40" name="payer_address" required="required"></textarea></span>
</div>
</div>

<div class="row">
<div class="col-md-4">
	<span class="wpcf7-form-control-wrap your-name"><input type="text" placeholder="State*" class="" size="40" value="" name="payer_state" required="required"></span>
</div>

<div class="col-md-4">
	<span class="wpcf7-form-control-wrap mobile-number"><input type="text" placeholder="City*" class="" size="40" value="" name="payer_city" required="required"></span>
</div>

<div class="col-md-4">
	<span class="wpcf7-form-control-wrap mobile-number"><input type="text" placeholder="Pin*"  class="" size="40" value="" name="payer_pin" required="required"></span>
</div>
</div>

<div class="row">
<div class="col-md-3">
	<label>I wish to Donate</label>
	
	<span class="wpcf7-form-control-wrap your-name"><span class="inr_icon"><img src="http://nanhijaan.com/wp-content/themes/Impreza/img/icn_rupe.png"></span><input type="number" placeholder="Amount*" required  class="" size="40" value="" name="payer_amount"></span>
</div>
<div class="col-md-9">
&nbsp;
</div>
</div>


<div class="row">
<div class="col-md-12">
	<input type="hidden" value="'.$purpose.'" name="payer_purpose" >
	<input type="submit" class="" value="Make a Payment" name="submit">
</div>
</div>

</div>
</form>
</div>
</div>
</div>';
    
}
add_shortcode('online_payment_shortcode','online_donate_payment');	
/*END ABOVE ONLINE WEBFORM  CODE HERE*/



class Menu_Item{
    private $object;
    private $object_id;
    private $link_name;
    private $link_value;

    public function __construct($object, $object_id, $linkname, $linkvalue){
      $this->object_id = $object_id;
      $this->object = $object;
      $this->link_name = $linkname;
      $this->link_value = $linkvalue;
    }

    public function get_link_name(){
      return $this->link_name;
    }

    public function get_object_id(){
      return $this->object_id;
    }

    public function get_link_value(){
      return $this->link_value;
    }

    public function get_object(){
      return $this->object;
    }
}


// add_action('admin_menu', 'menusapisystem');

// function menusapisystem() {
//     // Add a new top-level menu (ill-advised):
//     add_menu_page(__('menusapisystem','menusapiAdmin'), __('Menu API System','menusapiAdmin'), '', 'rs-top-level-handle', 'rs_toplevel_page' );
// 	add_submenu_page('rs-top-level-handle', __('menusapisystem','menu-rugssystem'), __('Menu API System','menu-rugssystem'), 'manage_options', 'menusapi', 'menusapi');
// }



function hidebanner() {
	if ($_GET['hide']=='banner')
	{
    	echo "<script>
		jQuery('.page section.about-header').remove();
		jQuery ('.w-logo').remove();
		jQuery ('.w-nav').remove();
		</script>";
	}
}
add_action( 'wp_footer', 'hidebanner', 100 ); 

function tabsystem()
{
	if ($_GET['section']!='')
	{
		echo "<script>
		jQuery('.w-tabs-section').hide();
		jQuery('.w-tabs-section-header').remove();
		jQuery('.w-tabs-list.items_4').remove();
		jQuery('.w-tabs-section.".$_GET['section']."').addClass('active');
		jQuery('.w-tabs-section.".$_GET['section']."').show();
		jQuery('.w-tabs-section.".$_GET['section']."').removeClass('w-tabs-section');
		jQuery('.".$_GET['section']." .w-tabs-section-content').attr('style', 'display: block !important');
		</script>";
	}
}
add_action( 'wp_footer', 'tabsystem', 100); 

function menusapi()
{
	//echo "<h1>Menus API</h1>";

	$menu_name = $_GET['menu']; //Main Menu'; //menu name slug
	$hidebanner = $_GET['hide'];
	$menu_json_data_string = '[';

	if ($menu_name) {
	  $menu = wp_get_nav_menu_object( $menu_name );

	  $menu_items = wp_get_nav_menu_items($menu->term_id);

	  //initialize two menu item arrays
	  $main_menu_item_array = array();
	  $sub_menu_item_array = array();//defined as 2d array
	  //define two menu item arrays first
	  for($i=0;$i<count($menu_items);$i+=1){
	    $current_menu_item_obj = $menu_items[$i];

	    if($current_menu_item_obj->menu_item_parent == "0"){ //main menu item found
	      //define a new menu_item object
	      $menu_item_obj = new Menu_Item($current_menu_item_obj->object, $current_menu_item_obj->object_id, $current_menu_item_obj->title, $current_menu_item_obj->url);
	      $main_menu_item_array[$current_menu_item_obj->ID] = $menu_item_obj;
	    }else{ //sub menu found
	      $menu_item_obj = new Menu_Item($current_menu_item_obj->object, $current_menu_item_obj->object_id, $current_menu_item_obj->title, $current_menu_item_obj->url);
	      $sub_menu_item_array[$current_menu_item_obj->menu_item_parent][] = $menu_item_obj;
	    }
	  }

	  $menu_item_json_array = array();
	  foreach($main_menu_item_array as $each_main_menu_key=>$each_main_menu_item){

	  	if ($hidebanner=='banner') { $hide = '?hide=banner'; } else { $hide=''; }

	    $each_menu_item_link_name = $each_main_menu_item->get_link_name();
	    $each_menu_item_link_value = $each_main_menu_item->get_link_value().$hide;
	    $each_menu_item_object_id = $each_main_menu_item->get_object_id();
	    $each_menu_item_slug = strtolower(str_replace(" ", "_", $each_menu_item_link_name));
	    $each_menu_item_slug = str_replace("`", "", $each_menu_item_slug);
	    $obj = $each_main_menu_item->get_object();
	    $type = ($obj=='meta_category')?'meta':'main';
		//$content_post = get_post($each_menu_item_object_id);
		//$content = $content_post->post_content;
		//$content = apply_filters('the_content', $content);
	    $each_menu_item_string = '{"label" : "'.$each_menu_item_slug.'", "id": "'.$each_menu_item_object_id.'", "type": "'.$type.'", "url":"'.$each_menu_item_link_value.'","name":"'.$each_menu_item_link_name.'"';

	    if(array_key_exists($each_main_menu_key, $sub_menu_item_array)){ //has sub menu item
	      //count submenu items
	      $count_sub_menu_item = count($sub_menu_item_array[$each_main_menu_key]);
	      //put each sub menu item into an array
	      $submenu_item_list_main_string = ',"children": [';
	      $submenu_item_list_array = array();
	      foreach($sub_menu_item_array[$each_main_menu_key] as $each_sub_menu_item){

	      	if ($hidebanner=='banner') { $hidechild = '&hide=banner'; } else { $hidechild=''; }

	        $obj = $each_sub_menu_item->get_object();
	        $type = ($obj=='meta_category')?'meta':'main';
			//$content_post = get_post($each_sub_menu_item->get_object_id());
			///$content = $content_post->post_content;
			//$content = apply_filters('the_content', $content);
	        $each_sub_menu_item_string = '{"id": "'.$each_sub_menu_item->get_object_id().'", "type": "'.$type.'", "url":"'.$each_sub_menu_item->get_link_value().$hidechild.'", "name":"'.$each_sub_menu_item->get_link_name().'"}';
	        $submenu_item_list_array[] = $each_sub_menu_item_string;
	      }
	      $submenu_item_list_string = implode(",", $submenu_item_list_array);
	      $submenu_item_list_main_string .= $submenu_item_list_string;
	      $submenu_item_list_main_string .= ']';
	      $each_menu_item_string .= $submenu_item_list_main_string;
	    }
	    $each_menu_item_string .= '}';
	    $menu_item_json_array[] = $each_menu_item_string;
	  }
	  $menu_item_json_string = implode(",", $menu_item_json_array);
	}

	$menu_json_data_string .= $menu_item_json_string;
	$menu_json_data_string .= ']';

	echo $menu_json_data_string;

}

add_shortcode('menusapi', 'menusapi');

if('yes' == $_GET['test_email']){
	$headers = "From:  Nanhijaan <help@biggtruck.in> \r\n " ;
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
	
	if(wp_mail( 'harjeet.s@signitysolutions.in', 'Testing Subject', '<b>Testing Message</b>', $headers)){
		echo "Mail function working properly";
	} else {
		echo "Mail function not working properly";
	}
	
}

//add_filter( 'wp_mail_content_type', 'set_content_type' );
function set_content_type( $content_type ) {
	return 'text/html';
}
