<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );
/*
Template Name: Gallery
*/
$us_layout = US_Layout::instance();

get_header();
us_load_template( 'templates/titlebar' );
$pageid		= '404';
$header_img_url	=	wp_get_attachment_url( get_post_thumbnail_id($post->ID) );

?>
<!-- MAIN -->
<div class="l-main">
	<div class="l-main-h i-cf">
		<div class="l-content g-html">
			<?php do_action( 'us_before_page' ) ?>
			
			<section class="l-section wpb_row height_medium imgsize_cover with_img about-header vc_row-fluid">
				<div style="background-image:url(<?php echo $header_img_url ;?>)" class="l-section-img"></div>
						<div class="l-section-h g-html i-cf">
							<div class="g-cols offset_small">
								<div class=" full-width">
									<h1 class="vc_custom_heading" style="font-size: 53px;color: #ffffff;text-align: left;font-family:Lato;font-weight:300;font-style:normal"><?php echo $post->post_title ; ?></h1>
								</div>
							</div>
						</div>
					
			</section>
			
			<div class="container gallerysection">
				<div class="row">
					<?php
						$paged 			=	(get_query_var('paged')) ? get_query_var('paged') : 1	;
						$limit 			= 	get_option('posts_per_page');		
						query_posts('post_type=gallery&posts_per_page'.$limit.'&paged='.$paged.'&orderby=date');
							if (have_posts()) : 
								while ( have_posts() ) : the_post();
					
					?>
						<div class="col-lg-4 col-sm-6 col-xs-12" style="padding-bottom: 70px;">	
							<div class="single_latest">					
								<?php
								if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
									$img_url	=	wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
								} else {
									$img_url	=	home_url().'/wp-content/uploads/2015/07/no_image.jpeg';
								}
								?>
								<div class="single_image">
									<img class="img-responsive" height="300px" src="<?php echo $img_url;?>" >
								</div>
				
								<div class="title_description">
									<center>
										<a class="title-head" href="<?php echo the_permalink(); ?>"><?php the_title();?></a><br/>
										<?php the_time('l, F jS, Y') ?>
									</center>
									<p>
										<?php 
										if(strlen($post->post_content) > 150 ){
											echo substr($post->post_content,0,150).'...';
										} else {
											echo $post->post_content;
										}																	
										?>
										<br/>
									</p>
									<a href="<?php the_permalink(); ?> "class="hubreadmore">Read More -></a>
									<br/>
							</div>
						</div>
					</div>

				<?php 
				endwhile;
				endif;

				?>
				<div class="navigationgernator_class"><?php wp_pagenavi(); ?></div>
			</div>
		</div>
	</div>

		
	</div>
</div>

<style>
.title-head{color: #d1692a;}
</style>

<?php get_footer() ?>
