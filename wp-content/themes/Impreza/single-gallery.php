<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );
/**
 * The template for displaying all single posts and attachments
 */
$us_layout = US_Layout::instance();
// Needed for canvas class
//$us_layout->titlebar = 'default';
get_header();
$pageid		= '404';
$header_img_url	=	wp_get_attachment_url( get_post_thumbnail_id($pageid) );
$template_vars = array(
	'metas' => (array) us_get_option( 'post_meta', array() ),
	'show_tags' => in_array( 'tags', us_get_option( 'post_meta', array() ) ),
);
?>
<!-- MAIN -->
<div class="l-main">
	<div class="l-main-h i-cf">

		<div class="l-content g-html">

			<?php do_action( 'us_before_single' ) ?>
			
			<section class="l-section wpb_row height_medium imgsize_cover with_img about-header vc_row-fluid">
				<div style="background-image:url(<?php echo $header_img_url ;?>)" class="l-section-img"></div>
						<div class="l-section-h g-html i-cf">
							<div class="g-cols offset_small">
								<div class=" full-width">
									<h1 class="vc_custom_heading" style="font-size: 53px;color: #ffffff;text-align: left;font-family:Lato;font-weight:300;font-style:normal"><?php echo $post->post_title ; ?></h1>
								</div>
							</div>
						</div>
					
			</section>

			<?php
			while ( have_posts() ){
				the_post();

				us_load_template( 'templates/blog/single-post', $template_vars );
			}
			?>

			<?php do_action( 'us_after_single' ) ?>

		</div>

		<?php if ( $us_layout->sidebar_pos == 'left' OR $us_layout->sidebar_pos == 'right' ): ?>
			<aside class="l-sidebar at_<?php echo $us_layout->sidebar_pos ?>">
				<?php generated_dynamic_sidebar(); ?>
			</aside>
		<?php endif; ?>

	</div>
</div>

<?php get_footer(); ?>
