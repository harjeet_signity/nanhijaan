<?php

//// Below function is used to convert html reciept into fdf reciept
function create_pdf($payment_ID, $name, $address, $amount, $filename, $amount_words) {
    //============================================================+
	// File name   : example_001.php
	// Begin       : 2008-03-04
	// Last Update : 2013-05-14
	//
	// Description : Example 001 for TCPDF class
	//               Default Header and Footer
	//
	// Author: Nicola Asuni
	//
	// (c) Copyright:
	//               Nicola Asuni
	//               Tecnick.com LTD
	//               www.tecnick.com
	//               info@tecnick.com
	//============================================================+

	/**
	 * Creates an example PDF TEST document using TCPDF
	 * @package com.tecnick.tcpdf
	 * @abstract TCPDF - Example: Default Header and Footer
	 * @author Nicola Asuni
	 * @since 2008-03-04
	 */

	// Include the main TCPDF library (search for installation path).
	require_once('convert_pdf/examples/tcpdf_include.php');
	// create new PDF document
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

	// set header and footer fonts
	$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

	// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

	// set margins
	$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
	$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

	// set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

	// set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
	
	// ---------------------------------------------------------

	// set default font subsetting mode
	$pdf->setFontSubsetting(true);

	// Set font
	// dejavusans is a UTF-8 Unicode font, if you only need to
	// print standard ASCII chars, you can use core fonts like
	// helvetica or times to reduce file size.
	$pdf->SetFont('dejavusans', '', 14, '', true);

	// Add a page
	// This method has several options, check the source code documentation for more information.
	$pdf->AddPage();

	// set text shadow effect
	//$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

	// Set some content to print
	$html = '<table cellpadding="5" cellspacing="0" width="600" style="font-family: Helvetica, Arial, sans-serif; font-size: 12px;">';
	$html .=	'<tr style="padding:20px 0px 0px 0px;"  bgcolor="#FFFDF2">';
	$html .=		'<td style="padding: 5px 0px 5px 0px;">
						<tr>
							<td bgcolor="#FFFDF2" colspan="2" style="padding: 0px 0px 0px 0px; font-size: 32px; font-family: Helvetica, Arial, sans-serif; font-weight: bold; color: #0A3775; text-shadow: 2px 2px 2px FFFDF2; text-align: center;"><img src="http://nanhijaan.com/wp-content/uploads/2015/12/NJ-logo-website-04-300x160.png" width="150px"><br/>Veeranwali Foundation - Nanhi Jaan</td>
						</tr>
						<tr>
							<td colspan="2" align="center" style="padding: 0px 45px 0px 45px; font-size: 16px; line-height: 22px; font-family: Helvetica, Arial, sans-serif; color: #062145;">SCO 343-345, Sector 34-A, Chandigarh</td>
						</tr>
					</td>
				</tr>';
	$html .=	'<tr>
					<td width="250" >
						No: '.$payment_ID.'
					</td>
					<td width="350"  style="text-align: right;">
						Date: '.date("j F, Y").'
					</td>							
				 </tr>
				 <tr>
					<td width="250" style="font-size: 12px;">
						Received with thanks from:
					</td>
					<td width="350" style="border-bottom:1px dotted #000;">
						'.$name.'<br/>------------------------------------------------------------------------------------
					</td>				
				 </tr>	 			
				 <tr>
					<td width="250">
						Address:
					</td>
					<td width="350">
						'.$address.'<br/>------------------------------------------------------------------------------------ 
					</td>				
				 </tr>
				 <tr>
					<td width="250" style="padding: 25px;">
						Amount (Rs):
					</td>
					<td width="350" style="border-bottom: 1px dotted #000;">
						'.$amount.'<br/>------------------------------------------------------------------------------------
					</td>				
				 </tr>
				 <tr>
					<td width="250" style="padding: 25px;">
						Amount in Words (Rs) :
					</td>
					<td width="350" style="border-bottom: 1px dotted #000;">
						'.$amount_words.' rupees only.<br/>------------------------------------------------------------------------------------
					</td>				
				 </tr>
				 <tr>
					<td width="250">
						
					</td>
					<td width="350" style="border-bottom: 1px dotted #000;">
						<i>This is a digitally generated receipt and does not require any signature.</i><br/>
					</td>				
				 </tr>';
	$html .='</table>';  
	$html .='<table align="center" bgcolor="#E5E4D0" cellpadding="5" cellspacing="0" width="600" style="padding:0px 5px 0px 5px; border: 1px solid #000;background: white;">
				<tr style="padding:30px;">
					<td align="left" style="font-family: Helvetica, Arial, sans-serif; color: #404040; font-size: 14px; padding: 10px 15px 10px 15px; line-height: 18px; display: block; text-align: center; font-weight: bold;">Exempted under Section 80G of income Tax Act 1961 (order no. AA/CHD/2011-12/631 dt. 17.05.2012) Pan No. AAATV 9159 R</td>
				</tr>
			</table>';
			

	// Print text using writeHTMLCell()
	$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

	// ---------------------------------------------------------

	// Close and output PDF document
	// This method has several options, check the source code documentation for more information.
	//$pdf->Output('example_000.pdf', 'D');  // Open download popup
	//$pdf->Output(getcwd().'/download/output1'.date('m-d-Y-His').'.pdf', 'F');  // Save file to any folder location
	
	//ob_clean();
	
	// save file
	//echo getcwd();
	$pdf->Output(get_template_directory().'/pdf_reciepts/'.$filename.'.pdf', 'F');	
	//============================================================+
	// END OF FILE
	//============================================================+
}
function convertNumber($number)
{
	list($integer, $fraction) = explode(".", (string) $number);

	$output = "";

	if ($integer{0} == "-")
	{
		$output = "negative ";
		$integer    = ltrim($integer, "-");
	}
	else if ($integer{0} == "+")
	{
		$output = "positive ";
		$integer    = ltrim($integer, "+");
	}

	if ($integer{0} == "0")
	{
		$output .= "zero";
	}
	else
	{
		$integer = str_pad($integer, 36, "0", STR_PAD_LEFT);
		$group   = rtrim(chunk_split($integer, 3, " "), " ");
		$groups  = explode(" ", $group);

		$groups2 = array();
		foreach ($groups as $g)
		{
			$groups2[] = convertThreeDigit($g{0}, $g{1}, $g{2});
		}

		for ($z = 0; $z < count($groups2); $z++)
		{
			if ($groups2[$z] != "")
			{
				$output .= $groups2[$z] . convertGroup(11 - $z) . (
						$z < 11
						&& !array_search('', array_slice($groups2, $z + 1, -1))
						&& $groups2[11] != ''
						&& $groups[11]{0} == '0'
							? " and "
							: ", "
					);
			}
		}

		$output = rtrim($output, ", ");
	}

	if ($fraction > 0)
	{
		$output .= " point";
		for ($i = 0; $i < strlen($fraction); $i++)
		{
			$output .= " " . convertDigit($fraction{$i});
		}
	}

	return $output;
}

function convertGroup($index)
{
	switch ($index)
	{
		case 11:
			return " decillion";
		case 10:
			return " nonillion";
		case 9:
			return " octillion";
		case 8:
			return " septillion";
		case 7:
			return " sextillion";
		case 6:
			return " quintrillion";
		case 5:
			return " quadrillion";
		case 4:
			return " trillion";
		case 3:
			return " billion";
		case 2:
			return " million";
		case 1:
			return " thousand";
		case 0:
			return "";
	}
}

function convertThreeDigit($digit1, $digit2, $digit3)
{
	$buffer = "";

	if ($digit1 == "0" && $digit2 == "0" && $digit3 == "0")
	{
		return "";
	}

	if ($digit1 != "0")
	{
		$buffer .= convertDigit($digit1) . " hundred";
		if ($digit2 != "0" || $digit3 != "0")
		{
			$buffer .= " and ";
		}
	}

	if ($digit2 != "0")
	{
		$buffer .= convertTwoDigit($digit2, $digit3);
	}
	else if ($digit3 != "0")
	{
		$buffer .= convertDigit($digit3);
	}

	return $buffer;
}

function convertTwoDigit($digit1, $digit2)
{
	if ($digit2 == "0")
	{
		switch ($digit1)
		{
			case "1":
				return "ten";
			case "2":
				return "twenty";
			case "3":
				return "thirty";
			case "4":
				return "forty";
			case "5":
				return "fifty";
			case "6":
				return "sixty";
			case "7":
				return "seventy";
			case "8":
				return "eighty";
			case "9":
				return "ninety";
		}
	} else if ($digit1 == "1")
	{
		switch ($digit2)
		{
			case "1":
				return "eleven";
			case "2":
				return "twelve";
			case "3":
				return "thirteen";
			case "4":
				return "fourteen";
			case "5":
				return "fifteen";
			case "6":
				return "sixteen";
			case "7":
				return "seventeen";
			case "8":
				return "eighteen";
			case "9":
				return "nineteen";
		}
	} else
	{
		$temp = convertDigit($digit2);
		switch ($digit1)
		{
			case "2":
				return "twenty-$temp";
			case "3":
				return "thirty-$temp";
			case "4":
				return "forty-$temp";
			case "5":
				return "fifty-$temp";
			case "6":
				return "sixty-$temp";
			case "7":
				return "seventy-$temp";
			case "8":
				return "eighty-$temp";
			case "9":
				return "ninety-$temp";
		}
	}
}

function convertDigit($digit)
{
	switch ($digit)
	{
		case "0":
			return "zero";
		case "1":
			return "one";
		case "2":
			return "two";
		case "3":
			return "three";
		case "4":
			return "four";
		case "5":
			return "five";
		case "6":
			return "six";
		case "7":
			return "seven";
		case "8":
			return "eight";
		case "9":
			return "nine";
	}
}

?>
