<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );
/**
 * The template for displaying all single posts and attachments
 */
$us_layout = US_Layout::instance();
// Needed for canvas class
//$us_layout->titlebar = 'default';
get_header();
$pageid		= get_the_ID();
$header_img_url	=	wp_get_attachment_url( get_post_thumbnail_id($pageid) );
$template_vars = array(
	'metas' => (array) us_get_option( 'post_meta', array() ),
	'show_tags' => in_array( 'tags', us_get_option( 'post_meta', array() ) ),
);
?>
<!-- MAIN -->
<div class="l-main">
	<div class="l-main-h i-cf">
		<div class="l-content g-html">

			<?php do_action( 'us_before_single' ) ?>
			
			<section class="l-section wpb_row height_medium imgsize_cover with_img about-header vc_row-fluid">
				<div style="background-image:url(<?php echo $header_img_url ;?>)" class="l-section-img"></div>
						<div class="l-section-h g-html i-cf">
							<div class="g-cols offset_small">
								<div class=" full-width">
									<h1 class="vc_custom_heading" style="font-size: 53px;color: #ffffff;text-align: left;font-family:Lato;font-weight:300;font-style:normal"><?php echo $post->post_title ; ?></h1>
								</div>
							</div>
						</div>
					
			</section>

			<?php
			while ( have_posts() ){
				the_post();
				?>
				<section class="l-section wbp_row">
					<div class="l-section-h i-cf">
						<div class="w-blog-post-body">
							<h1><?php the_title(); ?></h1>
							<?php the_content(); ?>
						</div>
						<div class="w-tabs-section-content-h i-cf">
							<center>
								<div class="contact-middle-wrap">
									<h4 class="vc_custom_heading" style="color: #ffffff;text-align: center;font-family:Lato;font-weight:300;font-style:normal"><b>Fill the form to Donate Online :</b></h4><br/>
									<?php echo do_shortcode('[online_payment_shortcode purpose="'.get_the_title().'"]'); ?>
								</div>
							</center>
						</div>							
					</div>				
				</section>
				<?php
				
			}
			?>
			
			<?php do_action( 'us_after_single' ) ?>
			

		</div>

		<?php if ( $us_layout->sidebar_pos == 'left' OR $us_layout->sidebar_pos == 'right' ): ?>
			<aside class="l-sidebar at_<?php echo $us_layout->sidebar_pos ?>">
				<?php generated_dynamic_sidebar(); ?>
			</aside>
		<?php endif; ?>

	</div>
</div>
<style>
.single-donationcampaign .contact-middle-wrap {
	max-width : 820px;
}
/*
.single-donationcampaign .col-md-6 .mobile-number {
	margin : auto !important;
}
.single-donationcampaign .inr_icon {
	background: none repeat scroll 0 0 #333333;
	border-bottom-left-radius: 4px;
	border-top-left-radius: 4px;
	padding: 12px 15px;
	text-align: center;
}
.single-donationcampaign .row .col-md-3 .your-name input {
    margin-top: -6px;
    max-width: 118px;
}*/
</style>

<?php get_footer(); ?>
