<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );
/**
 * The template for displaying all single posts and attachments
 */
$us_layout = US_Layout::instance();
// Needed for canvas class
//$us_layout->titlebar = 'default';
get_header();
$pageid		= '425';
$header_img_url	=	wp_get_attachment_url( get_post_thumbnail_id($pageid) );
$template_vars = array(
	'metas' => (array) us_get_option( 'post_meta', array() ),
	'show_tags' => in_array( 'tags', us_get_option( 'post_meta', array() ) ),
);
?>
<!-- MAIN -->
<div class="l-main">
	<div class="l-main-h i-cf">
		<div class="l-content g-html">

			<?php do_action( 'us_before_single' ) ?>
			<section class="l-section wpb_row height_medium imgsize_cover with_img about-header vc_row-fluid">
				<div style="background-image:url(<?php echo $header_img_url ;?>)" class="l-section-img"></div>
				<div class="l-section-h g-html i-cf">
					<div class="g-cols offset_small">
						<div class=" full-width">
							<h1 class="vc_custom_heading" style="font-size: 53px;color: #ffffff;text-align: left;font-family:Lato;font-weight:300;font-style:normal"><?php echo $post->post_title ; ?></h1>
						</div>
					</div>
				</div>					
			</section>

			<?php
			while ( have_posts() ){
				the_post();
				us_load_template( 'templates/blog/single-post', $template_vars );	
					
			}
			
			$excluded_post_id 	=	get_the_ID();
			$category_detail  	=	get_the_category( $excluded_post_id );
			$category_id		=   $category_detail[0]->term_id;
			if($category_id == 2){
				$query_args = array(
					'post__not_in' 	=> array( $excluded_post_id ),
					'orderby'	   	=> 'date',
					'order'			=> 'DESC',
					'cat'			=> 2
				);
				$new = query_posts($query_args);
				if ( have_posts() ):
					?>
					<div class="container">
						<div class="row">
							<div class="col-md-12 related_news_section">
							<h1>Other News</h1>
							<ul>
							<?php
								while ( have_posts() ) :
									the_post();
									?>
										<li><a href="<?php the_permalink();?>" class="related_posts_link"><?php echo the_title(); ?></a></li>
									<?php						
								endwhile;
							?>
							</ul>
							</div>
						</div>
					</div>
					<?php
				endif;

				wp_reset_query();
			}
			if ( comments_open() OR get_comments_number() != '0' ): ?>
				<section class="l-section for_comments">
					<div class="l-section-h i-cf">
						<?php wp_enqueue_script( 'comment-reply' ) ?>
						<?php comments_template() ?>
					</div>
				</section>
			<?php endif;	
			?>			
			<?php do_action( 'us_after_single' ) ?>
			

		</div>

		<?php if ( $us_layout->sidebar_pos == 'left' OR $us_layout->sidebar_pos == 'right' ): ?>
			<aside class="l-sidebar at_<?php echo $us_layout->sidebar_pos ?>">
				<?php generated_dynamic_sidebar(); ?>
			</aside>
		<?php endif; ?>

	</div>
</div>
<?php get_footer(); ?>
<style>
	.breadchrub{
		display : none !important;
	}
	.breadcrumbsnew {
		display : block !important;
	}
</style>
