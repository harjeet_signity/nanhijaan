<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );
/*
Template Name: Make a Payment
*/
$us_layout = US_Layout::instance();

get_header();
us_load_template( 'templates/titlebar' ); 
$header_img_url	=	wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
$page_ID = get_the_ID();


/*INSTAMOJO PAYMENT GATEWAY CODE START HERE*/
require_once("instamojo/instamojo.php");
$api = new Instamojo('1f3e59added59cfc0ac29bad4af22f6d', '76a6226675471a0248016861dfcfad18');/*LIVE API KEY*/
//$api = new Instamojo('52e3920d198f02ba47598d701ead1e26', '9585588e0f6fc34b4e201441ca0c3291'); /*TESTNG API KEY*/

$server_valiation	=	array();
if(isset($_POST) and !empty($_POST) and $_POST['payer_amount']!=""){
	
	if($_POST['payer_amount']<10){
		$msg	=	"Please enter amount greater then equal to 10 INR!!";
	}else{	
		extract($_POST);
		try {
			$my_redirect_url = home_url().'/thanks-for-payment/';
			$response = $api->paymentRequestCreate(array(
				"purpose" => $payer_purpose,
				"amount" => $payer_amount,
				"send_email" => false,
				"email" => $payer_email,
				"send_sms"=>false,
				"phone"=>"+91".$payer_mobile,
				"buyer_name"=>$payer_name,
				"redirect_url" => $my_redirect_url
				));

				$PaymentRequestId				=	$response['id'];
				$redirectLink					=	$response['longurl'];
				$_POST['payment_request_id']	=	$PaymentRequestId;
				/*WRITE USER DATA IN FILE*/
				$themePath			= get_template_directory();
				$WriteUserInfo 		= fopen($themePath."/donate_sessions/".$PaymentRequestId.".txt", "w");
				$jsonEncodeUserInfo = json_encode($_POST);
				fwrite($WriteUserInfo, $jsonEncodeUserInfo);
				fclose($WriteUserInfo);
				
				//header('location:'.$redirectLink);
				?>
				<script>
				 window.location.href = "<?php echo $redirectLink;?>";
				</script>
				<?php
		}
		catch (Exception $e) {
			$paymentError	=	'Error: ' . $e->getMessage();
			$msg	=	 "<h2 class='confirmation-msg'><center>".$paymentError."<br/> Something is Wrong, please try again later!</center></h2>";
		}
	}
}else{
	$msg	=	"<h2 class='confirmation-msg'><center>Sorry, Unauthorised Page Access!</center></h2>";
}
/*INSTAMOJO PAYMENT GATEWAY CODE END HERE*/

?>
<!-- MAIN -->
<div class="l-main">
	<div class="l-main-h i-cf">
		<div class="l-content g-html">
			<?php do_action( 'us_before_page' ) ?>
			
			<section class="l-section wpb_row height_medium imgsize_cover with_img about-header vc_row-fluid">
				<div style="background-image:url(<?php echo $header_img_url ;?>)" class="l-section-img"></div>
						<div class="l-section-h g-html i-cf">
							<div class="g-cols offset_small">
								<div class=" full-width">
									<h1 class="vc_custom_heading" style="font-size: 53px;color: #ffffff;text-align: left;font-family:Lato;font-weight:300;font-style:normal"><?php echo $post->post_title ; ?></h1>
								</div>
							</div>
						</div>
					
			</section>
			
		<div class="container">
			<div class="row">
			<?php
			if(strlen($msg)>5){
					echo '<span style="color:#F00;">'.$msg.'</span>';
			}else{
				echo '<h2 class="confirmation-msg"><center>Processing...</center></h2>';
			}	
			?>	
			</div>
		</div>
	</div>

		
	</div>
</div>

<style>
.title-head{color: #d1692a;}
.confirmation-msg{margin: -120px 0px 40px 0px !important;}
</style>

<?php get_footer() ?>
