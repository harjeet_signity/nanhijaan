<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );
/*
Template Name: Blogs
*/
$us_layout = US_Layout::instance();

get_header();
us_load_template( 'templates/titlebar' );
$header_img_url	=	wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
?>
<!-- MAIN -->

<div class="l-main">
	<div class="l-main-h i-cf">
		<div class="l-content g-html">
			<?php do_action( 'us_before_page' ) ?>
			
			<section class="l-section wpb_row height_medium imgsize_cover with_img about-header vc_row-fluid">
				<div style="background-image:url(<?php echo $header_img_url ;?>)" class="l-section-img"></div>
						<div class="l-section-h g-html i-cf">
							<div class="g-cols offset_small">
								<div class=" full-width">
									<h1 class="vc_custom_heading" style="font-size: 53px;color: #ffffff;text-align: left;font-family:Lato;font-weight:300;font-style:normal"><?php echo $post->post_title ; ?></h1>
								</div>
							</div>
						</div>
					
			</section>
			
			<div class="container blogsection">
				
					<?php
						$paged 			=	(get_query_var('paged')) ? get_query_var('paged') : 1	;
						$limit 			= 	get_option('posts_per_page');
						
                                                $args =array(   'post_type' => 'post',
								'category_not_in' => array(2),
								'posts_per_page' => $limit,
								'paged' => $paged,
								'orderby'=> 'date'
								);
                                                //query_posts($args);				
						query_posts('post_type=post&posts_per_page='.$limit.'&cat=-2&paged='.$paged.'&orderby=date');
							if (have_posts()) : 
								while ( have_posts() ) : the_post();
					
					?>
						<div class="row" style="padding-bottom:45px;">
							<div class="col-lg-3 col-sm-3 col-xs-3">
									<?php
									if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
										$img_url	=	wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
									} else {
										$img_url	=	home_url().'/wp-content/themes/Impreza/img/images.jpeg';
									}
									?>
									
								<img class="img-responsive" height="300px" src="<?php echo $img_url;?>" >
								
							</div>
							<div class="col-lg-9 col-sm-9 col-xs-9">
									
									<div class="title_description">
										<a class="title-head" href="<?php echo the_permalink(); ?>"><?php the_title();?></a><br/>
										<div class="w-blog-post-meta">
											<time class="w-blog-post-meta-date date updated"><?php echo get_the_date() ;?></time>
											<span class="w-blog-post-meta-author vcard author">
												<span class="fn"><?php echo get_the_author();?></span>
											</span>
											<span class="w-blog-post-meta-comments">
												<?php
												$comments_number = get_comments_number();
												$comments_label = sprintf( _n( '%s comment', '%s comments', $comments_number, 'us' ), $comments_number );
												comments_popup_link( __( 'No comments', 'us' ), $comments_label, $comments_label );
												 ?>
											</span>			
									</div>
										<?php //the_time('l, F jS, Y') ?>
										
										<p>
											<?php 
											if(strlen($post->post_content) > 150 ){
												echo substr($post->post_content,0,150).'...';
											} else {
												echo $post->post_content;
											}																	
											?>
											<br/>
										</p>
										<a href="<?php the_permalink(); ?> "class="btn btn-success">Read More</a>
										<br/>
								</div>
							
						</div>
					</div>
				<?php 
				endwhile;
				endif;

				?>
				<div class="navigationgernator_class"><?php wp_pagenavi(); ?></div>
			</div>
		</div>
	</div>

		
	</div>
</div>

<style>
.title-head{color: #d1692a;}
</style>

<?php get_footer() ?>
