<?php

require_once('class.phpmailer.php');
include("class.smtp.php"); // optional, gets called from within class.phpmailer.php if not already loaded

class SMTP_mail extends PHPMailer {
	function send_smtp_mail($fromAddress, $fromName, $bccAddress, $bccName, $subject, $message, $toAddress, $toName, $attachement) {
		$this->SetFrom($fromAddress, $fromName);
		$this->addBCC($bccAddress, $bccName);
		$this->Subject    = $subject;
		$this->MsgHTML($message);
		$this->AddAddress($toAddress, $toName);
		$this->AddAttachment($attachement);
              
		if($this->Send()) {
		//	echo "Success: " . time();
			return true;
		}
		else {
		//	echo "Mailer Error: " . $this->ErrorInfo;
			return false;
		}
	}
}

/// Creating Object
$mail             = new SMTP_mail();

/// Setting SMTP gmail details
$mail->IsSMTP(); // telling the class to use SMTP
$mail->Host       = "smtp.gmail.com"; // SMTP server
$mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
										   // 1 = errors and messages
										   // 2 = messages only
$mail->SMTPAuth   = true;                  // enable SMTP authentication
$mail->SMTPSecure = 'ssl';
$mail->Port       = 465;                    // set the SMTP port for the GMAIL server

/////// Email id Credentials
$mail->Username   = "donotreplyonme@gmail.com"; // SMTP account username
$mail->Password   = "signity@123";        // SMTP account password	

///// Add Cc, Bcc and reply
//$mail->addCC('rohit@signitysolutions.com', 'Rohit Kaushal');
//$mail->addBCC('rohit@signitysolutions.com', 'Rohit Kaushal');
//$mail->AddReplyTo('helpdesk@nanhijaan.in', 'Help Desk');

//// Add attachement
//$mail->AddAttachment("images/phpmailer.gif");      // attachment

?>    
