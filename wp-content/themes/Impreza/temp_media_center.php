<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );
/*
Template Name: Media Center
*/
$us_layout = US_Layout::instance();

get_header();
us_load_template( 'templates/titlebar' ); 
$header_img_url	=	wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
$page_ID = get_the_ID();
?>
<!-- MAIN -->
<div class="l-main">
	<div class="l-main-h i-cf">
		<div class="l-content g-html">
			<?php do_action( 'us_before_page' ) ?>
			
			<section class="l-section wpb_row height_medium imgsize_cover with_img about-header vc_row-fluid">
				<div style="background-image:url(<?php echo $header_img_url ;?>)" class="l-section-img"></div>
					<div class="l-section-h g-html i-cf">
						<div class="g-cols offset_small">
							<div class=" full-width">
								<h1 class="vc_custom_heading" style="font-size: 53px;color: #ffffff;text-align: left;font-family:Lato;font-weight:300;font-style:normal"><?php echo $post->post_title ; ?></h1>
							</div>
						</div>
					</div>					
			</section>
			
			<div class="page_content">
				<?php
					$footer_page = get_page($page_ID);
					echo apply_filters('the_content', $footer_page->post_content);
				?>	
			</div>
			
			<div class="in_the_media">
				<div class="container">
					<div class="row">
						<center><h1>In the Media</h1></center>
						<?php
							$args = array(
							  'post_type' => 'mediacenter',
							  'showposts' => 6,
							  'orderby'   => 'date',
								'tax_query' => array(
									array(
										'taxonomy' => 'tax_media_center',
										'field' => 'id',
										'terms' => array(5)
									)
								)
							);
							query_posts($args);
							//query_posts('post_type=mediacenter&showposts=5&cat=5&orderby=date');
								if (have_posts()) : 
									while ( have_posts() ) : the_post();
						
						?>
						<div class="col-lg-4 col-sm-6 col-xs-12" style="padding-bottom: 70px;">	
							<div class="media-item text-center">
								<div class="single_latest">					
									<?php
									if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
										$img_url	=	wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
									} else {
										$img_url	=	home_url().'/wp-content/uploads/2015/07/no_image.jpeg';
									}
									?>
									<div class="single_image">
										<a href="<?php echo $img_url ; ?>">
											<img class="img-responsive" height="300px" src="<?php echo $img_url;?>" >
										</a>
									</div>
								</div>
								<?php /* ?>
								<div class="title_description">
									
									<a class="title-head" href="<?php echo the_permalink(); ?>">
										<?php 
											if(strlen($post->post_title) > 25 ){
												echo substr($post->post_title,0,25).'...';
											} else {
												echo $post->post_title;
											}
										?>
									</a>
									<span class="media-item-date"><?php the_time('l, F jS, Y') ?></span>
									
									<p>
									<?php										
										if(strlen($post->post_content) > 150 ){
											echo substr($post->post_content,0,150).'...';
										} else {
											echo $post->post_content;
										}																	
									?>
										
									</p>									
									
								</div>	
								<?php */ ?>
									
							</div>			
						</div>
	
						<?php 
						endwhile;
						endif;
						wp_reset_postdata();
						?>
					</div>
				</div>
				<div class="organizational_news">
					<div class="container">
						<div class="row">
							<center><h1>Organizational News</h1></center>
							<?php
								$args = array(
								  'post_type' => 'mediacenter',
								  'showposts' => 3,
								  'orderby'   => 'date',
									'tax_query' => array(
										array(
											'taxonomy' => 'tax_media_center',
											'field' => 'id',
											'terms' => array(6)
										)
									)
								);
								query_posts($args);
								//query_posts('post_type=mediacenter&showposts=5&cat=5&orderby=date');
									if (have_posts()) : 
										while ( have_posts() ) : the_post();
							
							?>
							<div class="col-lg-4 col-sm-6 col-xs-12" style="padding-bottom: 70px;">	
								<div class="media-item text-center">
									<div class="single_latest">					
										<?php
										if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
											$img_url	=	wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
										} else {
											$img_url	=	home_url().'/wp-content/uploads/2015/07/no_image.jpeg';
										}
										?>
										<div class="single_image">
											<a href="<?php echo $img_url ; ?>">
												<img class="img-responsive" height="300px" src="<?php echo $img_url;?>" >
											</a>
										</div>
									</div>
									
									<?php /* ?>
									<div class="title_description">
										<a class="title-head" href="<?php echo the_permalink(); ?>"><?php the_title();?></a>
										<span class="media-item-date"><?php the_time('l, F jS, Y') ?></span>
										
										<p>
											<?php 
											if(strlen($post->post_content) > 150 ){
												echo substr($post->post_content,0,150).'...';
											} else {
												echo $post->post_content;
											}																	
											?>
											
										</p>									
										
									</div>	
									<?php */ ?>
													
								</div>
							</div>
		
						<?php 
						endwhile;
						endif;
						wp_reset_postdata();
						?>
							</div>						
						</div>		
				</div>
			</div>
</div>

<style>
.title-head{color: #d1692a;}
</style>

<?php get_footer() ?>
