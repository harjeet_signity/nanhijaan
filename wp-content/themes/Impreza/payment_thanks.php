<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );
/*
Template Name: Thanks For Payment
*/
$us_layout = US_Layout::instance();

get_header();
us_load_template( 'templates/titlebar' );
$pageid		= '404';
$header_img_url	=	wp_get_attachment_url( get_post_thumbnail_id($post->ID) );

require_once('pdf_generator.php');
//include "smtp_mail/smtp_mail.php";
?>
<!-- MAIN -->
<div class="l-main">
	<div class="l-main-h i-cf">
		<div class="l-content g-html">
			<?php //do_action( 'us_before_page' ) ?>
			
			<section class="l-section wpb_row height_medium imgsize_cover with_img about-header vc_row-fluid">
				<div style="background-image:url(<?php echo $header_img_url ;?>)" class="l-section-img"></div>
				<div class="l-section-h g-html i-cf">
					<div class="g-cols offset_small">
						<div class=" full-width">
							<h1 class="vc_custom_heading" style="font-size: 53px;color: #ffffff;text-align: left;font-family:Lato;font-weight:300;font-style:normal"><?php //echo $post->post_title ; ?></h1>
						</div>
					</div>
				</div>	
			</section>
			
			<div class="container gallerysection">
				<div class="row">
				<?php
					/*INSTAMOJO PAYMENT GATEWAY CODE START HERE*/
					require_once("instamojo/instamojo.php");
					$api = new Instamojo('1f3e59added59cfc0ac29bad4af22f6d', '76a6226675471a0248016861dfcfad18');/*LIVE API KEY*/
					//$api = new Instamojo('52e3920d198f02ba47598d701ead1e26', '9585588e0f6fc34b4e201441ca0c3291'); /*TESTNG API KEY*/
					
					if(isset($_REQUEST['payment_id']) and $_REQUEST['payment_id']!=""){
						
						/*READ DATA FROM FILE*/
						$PaymentRequestId	=	$_REQUEST['payment_request_id'];
						$themePath			=	get_template_directory();
						$UserJsonData		=	file_get_contents($themePath."/donate_sessions/".$PaymentRequestId.".txt");
						$UserArrayData		=	json_decode($UserJsonData);
										
						/*GET PAYMENT DETAIL*/
						$PaymentDetail = $api->paymentDetail($_REQUEST['payment_id']);
						/*echo "<pre>";
						print_r($PaymentDetail);*/
						
						//// Preparing parameters to call create pdf function
						$payment_id_f		= $PaymentDetail['payment_id'];
						$payer_name_f		= $PaymentDetail['buyer_name'];
						$payer_address_f	= $UserArrayData->payer_address.', '.$UserArrayData->payer_city.', '.$UserArrayData->payer_state.', PIN : '.$UserArrayData->payer_pin;
						$payment_amount_f	= $PaymentDetail['amount'];
						$payment_words_f	= convertNumber($payment_amount_f);
						
						//// Calling function to create pdf reciept
						create_pdf($payment_id_f, $payer_name_f, $payer_address_f, $payment_amount_f, $PaymentRequestId, $payment_words_f);
						$pdf_reciept	=	home_url().'/wp-content/themes/Impreza/pdf_reciepts/'.$PaymentRequestId.'.pdf';		
						if($PaymentDetail['status']=="Credit"){					
							$DonaterEmailId	=	$PaymentDetail['buyer_email'];
							$PaymentSub		=	"NanhiJaan Donation Detail";
							$mailTemplate	=	'<table width="99%" align="center">
							<tr>
							<th colspan="2"><h3>Thank you for your donation to Nanhi Jaan. You can download digital receipt by clicking <a href="'.$pdf_reciept.'" download="'.$PaymentRequestId.'.pdf">here</a></h3></th>
							</tr>

							<tr>
							<th>Payment Request ID:</th>
							<td>'.$_REQUEST['payment_request_id'].'</td>
							</tr>

							<tr>
							<th>Payment ID:</th>
							<td>'.$PaymentDetail['payment_id'].'</td>
							</tr>

							<tr>
							<th>Payment Status:</th>
							<td>'.$PaymentDetail['status'].'</td>
							</tr>

							<tr>
							<th>Name:</th>
							<td>'.$PaymentDetail['buyer_name'].'</td>
							</tr>

							<tr>
							<th>Email:</th>
							<td>'.$PaymentDetail['buyer_email'].'</td>
							</tr>
							
							<tr>
							<th>Phone:</th>
							<td>'.$PaymentDetail['buyer_phone'].'</td>
							</tr>

							<tr>
							<th>Amount (Rs):</th>
							<td>'.$PaymentDetail['amount'].'</td>
							</tr>
							
							<tr>
							<th>Address:</th>
							<td>'.$UserArrayData->payer_address.'</td>
							</tr>
							
							<tr>
							<th>City:</th>
							<td>'.$UserArrayData->payer_city.'</td>
							</tr>
							
							<tr>
							<th>State:</th>
							<td>'.$UserArrayData->payer_state.'</td>
							</tr>
							
							<tr>
							<th>Pin:</th>
							<td>'.$UserArrayData->payer_pin.'</td>
							</tr>

							</table>';
							// Always set content-type when sending HTML email
							$path			=	getcwd().'/wp-content/themes/Impreza/pdf_reciepts/';
							
							$fromAddress		=	"helpdesk@nanhijaan.in";
							$fromName		=	"Nanhijaan";
							$subject		=	$PaymentSub;
							$message		=	$mailTemplate;
							$toAddress		=	array(
												$DonaterEmailId,
												$fromAddress
											);
							$toName			=	$payer_name_f;
							$attachement		=	WP_CONTENT_DIR."/themes/Impreza/pdf_reciepts/".$PaymentRequestId.".pdf";
							
							if(isset($UserArrayData->payment_request_id) and !empty($UserArrayData->payment_request_id) and $UserArrayData->payment_request_id==$_REQUEST['payment_request_id']){
								
								$headers = "From:  Nanhijaan <helpdesk@nanhijaan.in> \r\n " ;
								$headers .= "MIME-Version: 1.0\r\n";
								$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
								//$attachments = array( $attachement );
								//add_filter('wp_mail_content_type',create_function('', 'return "text/html"; '));
								
								if(wp_mail( $toAddress, $subject, $message, $headers )){
									echo '<h2 class="confirmation-msg"><center>Thanks for the donation, we have sent your donation detail on your email ID!</center></h2>';						
								} else {
									echo '<h2 class="confirmation-msg"><center>Thanks for the donation.</center></h2>';						
								}							
							}
							
							if(!empty($PaymentRequestId)){
								?>
								<div class="w-btn-wrapper align_center">					
									<a href="<?php echo $pdf_reciept; ?>" class="w-btn style_solid size_medium color_red icon_none" target="_blank" style="background: none repeat scroll 0 0 #d1692a;
					padding: 15px 40px; margin: 0px 0px 30px 0px;" download="<?php echo $PaymentRequestId; ?>.pdf">Download E-Reciept</a>
								</div>
								<?php
							}
							//@unlink($themePath."/donate_sessions/".$PaymentRequestId.".txt");
						}else{
							echo '<h2 class="confirmation-msg"><center>Something went wrong...</center></h2>';
						}	
					}else{
						echo '<h2 class="confirmation-msg"><center>Sorry, Unauthorised page Access...</center></h2>';
					}
				?>	
				</div>
			</div>
			<?php //do_action( 'us_after_page' ) ?>
		</div>	
	</div>
</div>

<style>
.title-head{color: #d1692a;}
.confirmation-msg{margin: -120px 0px 40px 0px !important;}
</style>

<?php get_footer() ?>
