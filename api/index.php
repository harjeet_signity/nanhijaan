<?php
include '../wp-load.php';
include 'db_manager.php';
if( isset($_GET['url']) && !empty($_GET['url']) ) {
	$params						=	explode("/", $_GET['url']);
	$globalAry['operation']		=	strtolower( $params['0'] );
	$globalAry['method']		= 	strtolower( $params['1'] );
	$globalAry['id']			= 	strtolower( $params['2'] );
}
//global $wpdb ;
$objdbManager = new DBManager();
switch($globalAry['operation']) {
	// fetch pages records under about section in mobile device 
	case "getabout" :
		$page_id	=	$globalAry['id'];
		$objdbManager->getaboutRecord($globalAry['method'],$page_id);
	break;
	// fetch work page records for mobile device
	case "getwork" :
		$page_id	=	$globalAry['method'];	
		$objdbManager->getworkpageRecord($globalAry['method'],$page_id);
	break;
	
	case "getmediacenter":
		$type	=	$globalAry['method'];
		$id		=	$globalAry['id'];	
		$objdbManager->getMediaCenterRecord($globalAry['method'],$id);
		break;
	case "getgallery":
		$pageid		=	$globalAry['method'];
		$objdbManager->get_galleryRecord($pageid);
		break;
    case "getdonate":
		$pageid		=	$globalAry['method'];
		$objdbManager->get_donateRecord($pageid);
		break;
	case "getcontact":
		$objdbManager->get_contactRecord();
	   break;
}


?>
