<?php
class DBManager {
	
	function __construct()	{
	
	}
	//**************** getpageRecords function uses to fetch about sections records ****************
	
	function getaboutRecord($methodtype,$pageid){
		switch($methodtype) :
			//========== fetch vision page records=========
			case "vision":
				echo $this->get_VisionRecord($pageid);
			break;
			//========== fetch history page records=========
			case "history":
				echo $this->get_HistoryRecord($pageid);
			break;
			//========== fetch volunter page records=========
			case "volunteers":
				echo $this->get_VolunteerRecord($pageid);
			break;
			//========== fetch boardmemeber page records=========
			case "boardmembers":
				echo $this->get_BoardMemberRecord($pageid);
			break;
			
		endswitch;
	}
	
	//**************** get_VisionRecord function uses to fetch Vision sections records under About section  ****************
	
	function get_VisionRecord($pageid) {
		$page_arr					=	array();
		$page_arr['id'] 			=	$pageid;
		$page_arr['title']			=	'';
		$page_arr['description']	=	'';	
		if(get_page($pageid)):
			//$pagerecords 	= 	get_post($pageid,ARRAY_A); 
			if(get_field('vision_title',$pageid)):
				$page_arr['title']	=	get_field('vision_title',$pageid);
			endif;
			if(get_field('vision_description',$pageid)):
				$page_arr['description']	=	get_field('vision_description',$pageid);
			endif;
			$page_arr['status']		=	'1';
		else:
			$page_arr['status']		=	'0';
		endif;
		return $this->showResults($page_arr);
		die();
	}
	
	//**************** get_HistoryRecord function uses to fetch History sections records under About section  ****************
	
	function get_HistoryRecord($pageid) {
		$page_arr				=	array();
		$page_arr['id'] 		=	$pageid;
		$page_arr['title']			=	'';
		$page_arr['description']	=	'';
		if(get_page($pageid)):
		
			if(get_field('history_title',$pageid)):
				$page_arr['title']		=	get_field('history_title',$pageid);
			endif;
			if(get_field('history_decription',$pageid)):
				$page_arr['description']	=	get_field('history_decription',$pageid);
			endif;
			$page_arr['status']		=	'1';
		else:
			$page_arr['status']		=	'0';
		endif;
		return $this->showResults($page_arr);
		die();
	}
	
	//**************** get_VolunteerRecord function uses to fetch Volunteer sections records under About section  ****************
	
	function get_VolunteerRecord($pageid) {
		$page_arr				=	array();
		$page_arr['id'] 		=	$pageid;
		$page_arr['title']			=	'';
		$page_arr['description']	=	'';
		if(get_page($pageid)):
			//$pagerecords 		= 	get_post($pageid,ARRAY_A); 
			if(get_field('major_volunteers_title',$pageid)):
				$page_arr['title']		=	get_field('major_volunteers_title',$pageid);
			endif;
			if(get_field('major_volunteers_description',$pageid)):
				$page_arr['description']	=	get_field('major_volunteers_description',$pageid);
			endif;
			$page_arr['status']		=	'1';
		else:
			$page_arr['status']		=	'0';
		endif;
		
		return $this->showResults($page_arr);
		die();
	}
	
	//**************** get_VolunteerRecord function uses to fetch Volunteer sections records under About section  ****************
	
	function get_BoardMemberRecord($pageid) {
		$page_arr			=	array();
		$page_arr['id']		=	$pageid;
		 if(get_page($pageid)):
			$boardmember_arr 	= 	get_post_meta($pageid, 'boardmemeber', true);
			/*
			$sort 		=	array();
			foreach($boardmember_arr as $k=>$v):
				$sort['order-by'][$k]	=	$v['order-by'];
			endforeach;
			array_multisort($sort['order-by'], SORT_ASC,$boardmember_arr);
			*/
			foreach($boardmember_arr as $key=>$record):
				$boardmember_arr[$key]['image-upload']	=	wp_get_attachment_url($record['image-upload']);
			endforeach;
			$page_arr['title']			= 'Board MEMBER';
			$page_arr['description']	= $boardmember_arr;
			$page_arr['status']			=	'1';
		else:
			$page_arr['title']			= 'Board MEMBER';
			$page_arr['description']	= '';
			$page_arr['status']			=	'0';
		endif;
		return $this->showResults($page_arr);
		die();
	}
	
	//*******************  getworkpageRecord function is uses to fetch Our Work section records ****************************** 
	
	function getworkpageRecord($pageid) {
		//http_response_code(200);
		$page_arr			=	array();
		$page_arr['id']		=	$pageid;	
		//$pagerecords 		= 	get_post($pageid,ARRAY_A); 
		$page_arr['title']			=	'';
		$page_arr['description']	=	'';
		if(get_page($pageid)):
			if(get_field('mobile_title',$pageid)):
				$page_arr['title']		=	get_field('mobile_title',$pageid);
			endif;
			if(get_field('moblie-description',$pageid)):
				$page_arr['description']	=	get_field('moblie-description',$pageid);
			endif;
			$page_arr['status']			=	'1';
		else:
			$page_arr['status']			=	'0';
		endif;
		echo $this->showResults($page_arr);
		die();
	}
	
	//********************* showResults function is use to display records into json format **************************************
	
	
	//********************  Media tabs sections here ***************************************************
	
	
	function getMediaCenterRecord($type=null,$id=null){
		
		switch($type):
			case "themedia" :
				$this->get_themediaRecord();
			break;
			
			case "mediadetail" :
				$this->get_mediapostDetail($id);
			break;
			
			case "organizationnews" :
				$this->get_organizationnNewsRecord();
			break;
			
			case "organizationnews_detail" :
				$this->get_organzation_new_postDetail($id);
			break;
			
			case "stories":
				$this->get_storiesRecord();
			
			case "stories_detail":
			
				$this->getstories_postDetail($id);
				
		    case "pressrelease":
				$this->get_pressDetail($id);
			
		endswitch;
	}
	
	//================== get_themediaRecord function is use to fetch the media posts records Under Media section =======================
	
	function get_themediaRecord(){
		$media_arr			=	array();
		$page_arr			=	array();
		$page_arr['title']	=	'In the Media';
		$args = array(
						  'post_type' => 'mediacenter',
						   'orderby'   => 'date',
							'tax_query' => array(
								array(
									'taxonomy' => 'tax_media_center',
									'field' => 'id',
									'terms' => array(5)
								)
							)
						);
		query_posts($args);
		if ( have_posts() ) : 
			$m=0;
			while ( have_posts() ) : the_post();
				$postid							=	get_the_ID();
				$media_arr[$m]['id']			=	$postid;
				$media_arr[$m]['title']			=	get_the_title();
				$media_arr[$m]['date']			=	get_the_date();
				if ( has_post_thumbnail() ) { 
					// check if the post has a Post Thumbnail assigned to it.
					$image_url		=	wp_get_attachment_image_src( get_post_thumbnail_id($postid), 'thumbnail');
					$media_arr[$m]['image']		=	$image_url[0];
				} else {
					$media_arr[$m]['image']	=	home_url().'/wp-content/uploads/2015/07/no_image.jpeg';
				}
				$m++;
			endwhile;
		endif;
		$page_arr['description']		=	$media_arr;
		wp_reset_postdata();
		echo $this->showResults($page_arr);
		die();
	}
	
	//==================== get_mediapostDetail function is use to fetch media post detail record =============
	
	function get_mediapostDetail($postid){
		$page_arr					=	array();
		$page_arr['id']				=	$postid;	
		$page_arr['title']			=	'';
		$page_arr['description']	=	'';
		$categories 				=	get_the_terms($postid,'tax_media_center');
		//========= particular id exits any category or not =========
		if(!empty($categories)) :
			$cat_arr					=	array();
			foreach($categories as $catkey=>$cat_record):
				$cat_arr[]	=	$cat_record->term_id;
			endforeach;
			//========= post id exist on particular category =======
			if (in_array(5,$cat_arr)) :
				if(get_page($postid)):
					$postrecords 		= 	get_post($postid,ARRAY_A);
					$page_arr['title']	=	$postrecords['post_title'];
					if(get_field('mobile_post_description',$postid)):
						$page_arr['description']	=	get_field('mobile_post_description',$postid);
					endif;			
					$page_arr['date']			=	date("F j, Y",strtotime($postrecords['post_date']));				
					$page_arr['status']			=	'1';
				else:
					$page_arr['status']			=	'0';
				endif;
			else:
				$page_arr['status']			=	'0';
			endif;
		else :
			$page_arr['status']			=	'0';
		endif;
		//$this->pr($page_arr);
		echo $this->showResults($page_arr);
		die();
	}
	
	//======= get_organizationnNewsRecord function is use to fetch the Organization News  posts records Under Media section  ===============
	
	function get_organizationnNewsRecord() {
		$organization_arr		=	array();
		$page_arr				=	array();
		$page_arr['title']		=	'Oranization News';
		$args1 = array(
					  'post_type' => 'mediacenter',
						'orderby'   => 'date',
						'tax_query' => array(
							array(
								'taxonomy' => 'tax_media_center',
								'field' => 'id',
								'terms' => array(6)
							)
						)
					);
		query_posts($args1);
		if ( have_posts() ) : 
			$m=0;
			while ( have_posts() ) : the_post();
				$postid								=	get_the_ID();
				$organization_arr[$m]['id']			=	$postid;
				$organization_arr[$m]['title']		=	get_the_title();
				$organization_arr[$m]['date']		=	get_the_date();
				if ( has_post_thumbnail() ) { 
					// check if the post has a Post Thumbnail assigned to it.
					$image_url		=	wp_get_attachment_image_src( get_post_thumbnail_id($postid), 'thumbnail');
					$organization_arr[$m]['image']		=	$image_url[0];
				} else {
					$organization_arr[$m]['image']	=	home_url().'/wp-content/uploads/2015/07/no_image.jpeg';
				}
				$m++;
			endwhile;
		endif;
		$page_arr['description']		=	$organization_arr;
		wp_reset_postdata();
		echo $this->showResults($page_arr);
		die();
	}
	
	//==================== get_organzation_new_postDetail function is use to fetch Organization new post detail record under media	 =============
	
	function get_organzation_new_postDetail($postid){
		 
		$page_arr					=	array();
		$page_arr['id']				=	$postid;	
		$page_arr['title']			=	'';
		$page_arr['description']	=	'';
		$categories 				= get_the_terms($postid,'tax_media_center');
		//========= particular id exits any category or not =========
		if(!empty($categories)) :
			$cat_arr					=	array();
			foreach($categories as $catkey=>$cat_record):
				$cat_arr[]	=	$cat_record->term_id;
			endforeach;
			//========= post id exist on particular category =======
			if (in_array(6,$cat_arr)) :
				if(get_page($postid)):
					$postrecords 		= 	get_post($postid,ARRAY_A);
					$page_arr['title']	=	$postrecords['post_title'];
					if(get_field('mobile_post_description',$postid)):
						$page_arr['description']		=	get_field('mobile_post_description',$postid);
					endif;
					$page_arr['status']			=	'1';
				else:
					$page_arr['status']			=	'0';
				endif;
			else:
				$page_arr['status']			=	'0';
			endif;
		else :
			$page_arr['status']			=	'0';
		endif;
		echo $this->showResults($page_arr);
		die();
	}
	
	//======= get_storiesRecord function is use to fetch the Stories  posts records Under Media section  ===============
	
	public function get_storiesRecord(){
		
		$stories_arr			=	array();
		$page_arr			=	array();
		$page_arr['title']	=	'Stories';
		$args 				= 	array(
								  'post_type' => 'stories',
								   'orderby'   => 'date'
								);
		query_posts($args);
		if ( have_posts() ) : 
			$m=0;
			while ( have_posts() ) : the_post();
				$postid							=	get_the_ID();
				$stories_arr[$m]['id']			=	$postid;
				$stories_arr[$m]['title']		=	get_the_title();
				$stories_arr[$m]['date']		=	get_the_date();
				//$content 						=	get_the_content('Read more');
				
				if(get_field('story_mobile_description',$postid)):
					$stories_description			=	get_field('story_mobile_description',$postid);
					$stories_descripiton			=	substr(strip_tags($stories_description),0,100).' ----';
					$stories_arr[$m]['description']	=	$stories_descripiton;
				else:
					$stories_arr[$m]['description']	=	'';
				endif;
				if ( has_post_thumbnail() ) { 
					// check if the post has a Post Thumbnail assigned to it.
					$image_url		=	wp_get_attachment_image_src( get_post_thumbnail_id($postid), 'thumbnail');
					$stories_arr[$m]['image']		=	$image_url[0];
				} else {
					$stories_arr[$m]['image']	=	home_url().'/wp-content/uploads/2015/07/no_image.jpeg';
				}
				$m++;
			endwhile;
		endif;
		//$this->pr($stories_arr);
		$page_arr['description']		=	$stories_arr;
		wp_reset_postdata();
		echo $this->showResults($page_arr);
		die();
	}
	
	//==================== get_storiespostDetail function is use to fetch stories post detail record =============
	
	function getstories_postDetail($postid){
		
		$page_arr					=	array();
		$page_arr['id']				=	$postid;	
		$page_arr['title']			=	'';
		$page_arr['description']	=	'';
		
		if(get_page($postid)):
			$postrecords 		= 	get_post($postid,ARRAY_A);
			$page_arr['title']	=	$postrecords['post_title'];
			if(get_field('story_mobile_description',$postid)):
				$page_arr['description']	=	get_field('story_mobile_description',$postid);
			else:
				$page_arr['description']	=	'';
			endif;			
			$page_arr['date']			=	date("F j, Y",strtotime($postrecords['post_date']));				
			$page_arr['status']			=	'1';
		else:
			$page_arr['status']			=	'0';
		endif;
		echo $this->showResults($page_arr);
		die();
	}
	
	function get_pressDetail($pageid=null){
		$page_arr			=	array();
		$page_arr['id']		=	$pageid;	
		//$pagerecords 		= 	get_post($pageid,ARRAY_A); 
		$page_arr['title']			=	'';
		$page_arr['description']	=	'';
		if(get_page($pageid)):
			if(get_field('mobile_title',$pageid)):
				$page_arr['title']		=	get_field('mobile_title',$pageid);
			endif;
			if(get_field('moblie_description',$pageid)):
				$page_arr['description']	=	get_field('moblie_description',$pageid);
			endif;
			$page_arr['status']			=	'1';
		else:
			$page_arr['status']			=	'0';
		endif;
		echo $this->showResults($page_arr);
		die();
		
	}
	
	//******************  End of Media tabs section ****************************************************
	
	
	//*********************  Gallery api **************************************************************
	
	function get_galleryRecord($pageid = null) {
		global $wpdb;
		$page_arr					=	array();
		$page_arr['id']				=	$pageid;	
		$page_arr['title']			=	'Gallery';
		$page_arr['description']	=	'';
		if(get_page($pageid)):
			$data						= 	get_post_meta($pageid, 'speaker_meta_data');
			$gallery_arr			=	array();
			if(!empty($data)):
				$m	=	0;
				if(!empty($data[0])) :
					foreach($data[0] as $gallerykey=>$gallery_id):
						$gallery_rec 	=	$wpdb->get_results("SELECT wp_ngg_gallery.name,wp_ngg_gallery.title,wp_ngg_gallery.path,wp_ngg_pictures.* 
																FROM wp_ngg_gallery 
																INNER JOIN 
																wp_ngg_pictures 
																ON wp_ngg_gallery.gid = wp_ngg_pictures.galleryid 
																WHERE wp_ngg_pictures. galleryid = $gallery_id", ARRAY_A );
																
						foreach($gallery_rec as $key=>$records):
							$gallery_arr[$m]['galleryid']	=	$records['galleryid'];
							$gallery_arr[$m]['title']		=	$records['title'];
							$gallery_arr[$m]['largeimg']	=	home_url().$records['path'].'/'.$records['filename'];
							$gallery_arr[$m++]['smallimg']	=	home_url().$records['path'].'/thumbs/thumbs_'.$records['filename'];
						endforeach;
						
					endforeach;
				endif;
				$page_arr['description']	=	$gallery_arr;
			endif;
			$page_arr['status']			=	'1';
		else:
			$page_arr['status']			=	'0';
		endif;
		
		echo $this->showResults($page_arr);
		die();
	}
	
	//********************  End of Gallery api ********************************************************
	
	//================  get_donateRecord function is use to donate page ===============================
	
	function get_donateRecord($pageid) {
		
		$page_arr					=	array();
		$page_arr['id']				=	$pageid;	
		$page_arr['title']			=	'';
		$page_arr['description']	=	'';
		if(get_page($pageid)):
			if(get_field('mobile_title',$pageid)):
				$page_arr['title']		=	get_field('mobile_title',$pageid);
			endif;
			if(get_field('moblie_description',$pageid)):
				$page_arr['description']	=	get_field('moblie_description',$pageid);
			endif;
			$page_arr['status']			=	'1';
		else:
			$page_arr['status']			=	'0';
		endif;
		echo $this->showResults($page_arr);
		die();
		
	}
	
	//======================= End get_donateRecord function ===========================================
	
	//==================  get_contactRecord function is use for contact us ===========================
	
	function trimfun($a) {
		return trim($a);
	}
	
	function get_contactRecord(){
		/*
		$_POST['name']			=	'Rohit';
		$_POST['email']			=	'rohit@gmail.com';
		$_POST['mobileno']		=	'987654321';
		$_POST['education']		=	'M.C.A';
		$_POST['occupation']	=	'Software enginer';
		$_POST['address']		=	'House no 102, Housing board
									Chandigarh';
		*/
		$post			=	array_map("DBManager::trimfun",$_POST);
		$page_arr		=	array();
		$to				=	'rohit@signitysolutions.com';
		$subject		=	'Nanhijaan contact us form';
		$cc				=	'';
									
		if(!empty($_POST)) :
			$msg = "";
			foreach($_POST as $key=>$records):
				$msg .= "<p><strong>".ucwords($key)."	: </strong>".$_POST[$key]."</p>";	
			endforeach;
			$mailalert	=	$this->mailfun($to,$subject,$msg,$_POST['email'],$cc);
			
			if($mailalert):
				$page_arr['title']			=	'Request send';	
				$page_arr['description']	=	'Mail send sucessfully';
			else:
				$page_arr['title']			=	'Request not send';	
				$page_arr['description']	=	'Mail not send sucessfully';
			endif;
			$page_arr['status']			=	'1';
		else:
			$page_arr['description']	=	'Please insert records on all fields';
			$page_arr['status']			=	'0';
		endif;
		
		echo $this->showResults($page_arr);
		die();
		
		
	}
	
	//=============================================================================
	
	//=================== mailfun is use for mail send ============================
	
	function mailfun($to,$subject,$message,$from,$cc=null){
		// Always set content-type when sending HTML email
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		$headers .= 'From: <'.$from.'>' . "\r\n";
		
		if(!empty($cc)) {
			$headers .= 'Cc: '.$cc.'' . "\r\n";
		}
		
		if(mail($to,$subject,$message,$headers)) {
			return '1';
		} else {
			return '0';
		}
		
	}
	
	function pr($arr){
		echo '<pre>';
			print_r($arr);
		echo '</pre>';
	}
	
	
	function showResults($record_arr){
		header('Content-type: text/json');
        // header('Content-type: application/json');
        $data = str_replace('\/','/',json_encode($record_arr));
        return $data;
        
	}
}
?>
