<?php
//============================================================+
// File name   : example_001.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 001 for TCPDF class
//               Default Header and Footer
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Default Header and Footer
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).
require_once('tcpdf_include.php');
$name = "Harjeet Singh";
// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
//$pdf->SetCreator(PDF_CREATOR);
//$pdf->SetAuthor('Nicola Asuni');
//$pdf->SetTitle('TCPDF Example 001');
//$pdf->SetSubject('TCPDF Tutorial');
//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
//$pdf->setFooterData(array(0,64,0), array(0,64,128));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('dejavusans', '', 14, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

// set text shadow effect
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

// Set some content to print
$html = <<<EOD
		<style>
			.man_hdr{
				text-align: center;
				width: 100%;
				font-weight: bold;
				font-size: 42px;
			}

			.dat_ara{
				margin: 30px 0px;
			}

			.dat_ara .slip_no label{
				font-weight: bold;
				font-size: 24px;
				margin: auto;
				line-height: 48px;
			}

			.dat_ara .slip_no input{
				border: 0px;
				width: 210px;
				line-height: 38px;
				text-indent: 10px;
				font-size: 40px;
				margin-left: 20px;
			}

			.dat_ara .slip_dat label{
				font-weight: bold;
				font-size: 24px;
				margin: auto;
				line-height: 48px;
			}

			.dat_ara .slip_dat input{
				border: 0px;
				border-bottom: 3px dotted #6F6F6F;
				width: 170px;
				line-height: 38px;
				text-indent: 6px;
				font-size: 31px;
				margin-left: 20px;
			}

			.man_info{
				margin: auto;
				list-style: none;
				padding: 0px;
			}

			.man_info li{
				width: 100%;
				margin-bottom: 10px;
			}

			.man_info li:first-child label{
				max-width: 27%;
				margin-top: 20px;
				font-size: 24px;
				line-height: normal;
				margin-bottom: 0;
			}

			.man_info li:first-child input{
				width: 71%;
				border: 0px;
				border-bottom: 3px dotted #6F6F6F;
				line-height: 38px;
				text-indent: 6px;
				font-size: 31px;
			}


			.man_info li:last-child label{
				max-width: 26%;
				margin-top: 20px;
				font-size: 24px;
				line-height: normal;
				margin-bottom: 0;
			}

			.man_info li:last-child input{
				width: 77%;
				border: 0px;
				border-bottom: 3px dotted #6F6F6F;
				line-height: 38px;
				text-indent: 6px;
				font-size: 31px;
			}


			.man_info li:nth-child(3) label{
				max-width: 31%;
				margin-top: 20px;
				font-size: 24px;
				line-height: normal;
				margin-bottom: 0;
			}

			.man_info li:nth-child(3) input{
				width: 67%;
				border: 0px;
				border-bottom: 3px dotted #6F6F6F;
				line-height: 38px;
				text-indent: 6px;
				font-size: 31px;
			}

			.man_info li:nth-child(2) label{
				max-width: 26%;
				margin-top: 20px;
				font-size: 24px;
				line-height: normal;
				margin-bottom: 0;
			}

			.man_info li:nth-child(2) input{
				width: 88%;
				border: 0px;
				border-bottom: 3px dotted #6F6F6F;
				line-height: 38px;
				text-indent: 6px;
				font-size: 31px;
			}

			.man_info li:nth-child(4) label{
				max-width: 20%;
				margin-top: 20px;
				font-size: 24px;
				line-height: normal;
				margin-bottom: 0;
			}

			.man_info li:nth-child(4) input{
				width: 84%;
				border: 0px;
				border-bottom: 3px dotted #6F6F6F;
				line-height: 38px;
				text-indent: 6px;
				font-size: 31px;
			}

			.slip_sign label{
				width: 100%;
				text-align: right;
				font-size: 26px;
			}

			.totl_rat{
					margin-top: 68px;
			}

			.slip_botm p{
				padding: 6px 10px;
				border: 2px solid #565656;
				font-size: 19px;
				text-align: center;
				font-weight: bold;
				color: #565656;
			}
		</style>
		<div class="container">
			<div class="row">
				<div class="col-lg-12 man_hdr"><h1 style="font-weight:bold;">Veeranwali Foundation - Nanhi Jaan</h1><h3>SCO 343-345, Sector 34-A, Chandigarh</h3></div>
			</div>

			<div class="row  dat_ara">
				<div class="col-lg-4 col-md-4 pull-left slip_no"><label>No.</label><input type="text" /></div>
				<div class="col-lg-3 col-md-4 pull-right slip_dat"><label>Date.</label><input type="text"  class="pull-right" /></div>
			</div>

			<div class="row">
				<ul class="man_info">
					<li><label>Received with thanks from</label><input type="text" class="pull-right" /></li>
					<li><label>Address</label><input type="text" class="pull-right" /></li>
					<li><label>the sum of Rupees ( in words )</label><input type="text" class="pull-right" /></li>
					<li><label>on account of</label><input type="text" class="pull-right" /></li>
					<li><label>by Cash / Cheque No</label><input type="text" class="pull-right" /></li>
				</ul>
			</div>

			<div class="row  dat_ara">
				<div class="col-lg-3 col-md-3 pull-left slip_dat totl_rat"><label>Rs</label><input type="text" /></div>
				<div class="col-lg-3 col-md-4 pull-right slip_sign"><img src="njsplip/img/sign.png" /><label>Authorised Signatory</label></div>
			</div>

			<div class="row slip_botm">
				<p>Exempted under Section 80G of income Tax Act 1961 (order no. AA/CHD/2011-12/631 dt. 17.05.2012) Pan No. AAATV 9159 R</p>
			</div>

		</div>
EOD;

// Print text using writeHTMLCell()
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('example_000.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
?>
