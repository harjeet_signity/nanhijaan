<?php

function create_pdf($payment_ID, $name, $address, $amount) {
    //============================================================+
	// File name   : example_001.php
	// Begin       : 2008-03-04
	// Last Update : 2013-05-14
	//
	// Description : Example 001 for TCPDF class
	//               Default Header and Footer
	//
	// Author: Nicola Asuni
	//
	// (c) Copyright:
	//               Nicola Asuni
	//               Tecnick.com LTD
	//               www.tecnick.com
	//               info@tecnick.com
	//============================================================+

	/**
	 * Creates an example PDF TEST document using TCPDF
	 * @package com.tecnick.tcpdf
	 * @abstract TCPDF - Example: Default Header and Footer
	 * @author Nicola Asuni
	 * @since 2008-03-04
	 */

	// Include the main TCPDF library (search for installation path).
	require_once('tcpdf_include.php');
	// create new PDF document
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

	// set header and footer fonts
	$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

	// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

	// set margins
	$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
	$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

	// set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

	// set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		require_once(dirname(__FILE__).'/lang/eng.php');
		$pdf->setLanguageArray($l);
	}

	// ---------------------------------------------------------

	// set default font subsetting mode
	$pdf->setFontSubsetting(true);

	// Set font
	// dejavusans is a UTF-8 Unicode font, if you only need to
	// print standard ASCII chars, you can use core fonts like
	// helvetica or times to reduce file size.
	$pdf->SetFont('dejavusans', '', 14, '', true);

	// Add a page
	// This method has several options, check the source code documentation for more information.
	$pdf->AddPage();

	// set text shadow effect
	//$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

	// Set some content to print
	$html = '<table cellpadding="5" cellspacing="0" width="600" style="font-family: Helvetica, Arial, sans-serif; font-size: 12px;">';
	$html .=	'<tr style="padding:20px 0px 0px 0px;" >';
	$html .=		'<td style="padding: 5px 0px 5px 0px;">
						<tr>
							<td bgcolor="#FFFDF2" colspan="2" style="padding: 0px 0px 0px 0px; font-size: 32px; font-family: Helvetica, Arial, sans-serif; font-weight: bold; color: #0A3775; text-shadow: 2px 2px 2px FFFDF2; text-align: center;"><img src="http://dev2.signitysolutions.co.in/nanhijaan/wp-content/uploads/2015/12/300x160xNJ-logo-website-04-300x160.png.pagespeed.ic.qldXyN85rx.png" width="150px"><br/>Veeranwali Foundation - Nanhi Jaan</td>
						</tr>
						<tr>
							<td colspan="2" align="center" style="padding: 0px 45px 0px 45px; font-size: 16px; line-height: 22px; font-family: Helvetica, Arial, sans-serif; color: #062145;">SCO 343-345, Sector 34-A, Chandigarh</td>
						</tr>
					</td>
				</tr>';
	$html .=	'<tr>
					<td width="250" >
						No: '.$payment_ID.'
					</td>
					<td width="350"  style="text-align: right;">
						Date: '.date("j F, Y").'
					</td>							
				 </tr>
				 <tr>
					<td width="250" style="font-size: 12px;">
						Received with thanks from:
					</td>
					<td width="350" style="border-bottom: 1px dotted #000;">
						'.$name.'
					</td>				
				 </tr>	 			
				 <tr>
					<td width="250">
						Address:
					</td>
					<td width="350" style="border-bottom: 1px dotted #000;">
						'.$address.' 
					</td>				
				 </tr>
				 <tr>
					<td width="250" style="padding: 25px;">
						Sum of Rupees :
					</td>
					<td width="350" style="border-bottom: 1px dotted #000;">
						'.$amount.'
					</td>				
				 </tr>
				 ';
	$html .='</table>';  
	$html .='<table align="center" bgcolor="#E5E4D0" cellpadding="5" cellspacing="0" width="600" style="padding:0px 5px 0px 5px; border: 1px solid #000;background: white;">
				<tr style="padding:30px;">
					<td align="left" style="font-family: Helvetica, Arial, sans-serif; color: #404040; font-size: 14px; padding: 10px 15px 10px 15px; line-height: 18px; display: block; text-align: center; font-weight: bold;">Exempted under Section 80G of income Tax Act 1961 (order no. AA/CHD/2011-12/631 dt. 17.05.2012) Pan No. AAATV 9159 R</td>
				</tr>
			</table>';
			

	// Print text using writeHTMLCell()
	$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

	// ---------------------------------------------------------

	// Close and output PDF document
	// This method has several options, check the source code documentation for more information.
	//$pdf->Output('example_000.pdf', 'D');  // Open download popup
	//$pdf->Output(getcwd().'/download/output1'.date('m-d-Y-His').'.pdf', 'F');  // Save file to any folder location
	ob_clean();
	
	// save file
	//echo getcwd();
	$pdf->Output(getcwd().'/download/yourfile.pdf', 'F');	
	//============================================================+
	// END OF FILE
	//============================================================+
}

create_pdf("MOJO123", "Harjeet Singh", "Sector 17 Manimajra Chandigarh", 20);

?>
<html>
	<head>
	
	</head>
	<body>
		<!---<a href="download_pdf.php?file=youfile2">Download my eBook</a>-->
		<a href="http://localhost/convert_pdf/examples/download/yourfile.pdf" target="_blank" download="yourfile.pdf">Download the pdf</a>
	</body>
</html>
