<?php

class FunctionComponent extends Component{


function download($status,$estimate_id){
$modelEstimate      = ClassRegistry::init('Estimate');
$modelUser          = ClassRegistry::init('User');
$modelEstimateItem  = ClassRegistry::init('EstimateItem');
$modelCategory      = ClassRegistry::init('Category');
$modelImage         = ClassRegistry::init('Image');
$AdminInfo    = $modelUser->find('first',array('conditions'=>array('role_type'=>1)));	
$EstimateList = $modelEstimate->find('all',array('conditions'=>array('Estimate.id'=>$estimate_id)) );
if (!empty($EstimateList)) {
$servicetax	   = $EstimateList['0']['Estimate']['service_tax'];
$discount	   = $EstimateList['0']['Estimate']['discount'];
$admin_total_cost	= $EstimateList['0']['Estimate']['admin_total_cost'];
$servicecost	   = ($admin_total_cost)*($servicetax/100);
$adminservicecost	= ($admin_total_cost)+($servicecost);
$discountcost	   = ($adminservicecost)*($discount/100);
$admingrandcost	   = ($adminservicecost)-($discountcost);
$admingrandcost     = number_format($admingrandcost, 2); 
$EstimateList['0']['Estimate']['admin_grand_total_cost']	=	$admingrandcost;
$ItemsList = $modelEstimateItem->find('all',array('conditions'=>array('estimate_id'=>$estimate_id)) );
foreach($ItemsList as $key=>$item){
$item_id	   =	$item['EstimateItem']['id'];
$category_id	=	$item['EstimateItem']['category_id'];
$Categoryname	= $modelCategory->find('first', array('fields'=>'id,name','conditions'=>array('id'=>$category_id)));
$ItemsList[$key]['EstimateItem']['category_name']    =  $Categoryname['Category']['name'];
$itemimagecount	= $modelImage->find('count',array('conditions'=>array('id'=>$item_id,'estimate_id'=>$estimate_id,'isBefore'=>0)));
$ItemsList[$key]['EstimateItem']['image_count']    =  $itemimagecount;
$itemimagetotal = $modelImage->find('all', array('conditions'=>array('item_id'=>$item_id,'estimate_id'=>$estimate_id,'isBefore'=>0)));
$ItemsList[$key]['EstimateItem']['itemimagetotal'] = $itemimagetotal;
}
}
App::import('Vendor','tcpdf/tcpdf');
$tcpdf = new TCPDF();
$textfont = 'helvetica'; 
$tcpdf->SetAuthor("Labsdata");
$tcpdf->SetAutoPageBreak(true); 
$tcpdf->setPrintHeader(false);
$tcpdf->setPrintFooter(false);
$tcpdf->SetTextColor(0, 0, 0);
$tcpdf->SetFont($textfont,'',9); 
$tcpdf->AddPage('P', 'ANSI_A', false, false);  
$htmlcontent ='';
        $htmlcontent.='<table width="100%" cellpadding="0px" cellspacing="0px" style="border-collapse: collapse; width: 100%;"><tr><td><table width="100%" cellpadding="0" cellspacing="0" style="border-collapse: collapse; width: 100%;"><tr><td><img src="'.APP.'webroot/img/logo-transparent.png" align="left" width="180" height="52"></td></tr><tr><td><table width="100%" cellpadding="0" cellspacing="0" style="border-collapse: collapse; width: 100%;"><tr><td>'.$AdminInfo['User']['address'].'</td></tr><tr><td>'.$AdminInfo['User']['phone'].'</td></tr><tr><td>'.$AdminInfo['User']['website'].'</td></tr></table></td></tr></table></td>
                                <td>
                                    <table style="width: 100%; text-align: right; padding-top:1.2em;">
                                        <tr>
                                            <td style="font-size: 2em;">'.$status.'</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table style="width: 100%;">
                                                    <tr>
                                                        <td>Date: '.date("jS-F-Y",strtotime($EstimateList['0']['Estimate']['created'])).'</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Work Order: '.$EstimateList['0']['Estimate']['work_order'].'</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <table style="width: 100%;">
                            <tr>
                                <td style="height:30px;"></td>
                            </tr>
                        </table>
                        
                        <table style="width: 100%;">
                            <tr>
                                <td style="font-size:0.9em;">'.$status.' for:</td>
                            </tr>
                            <tr>
                                <td style="font-size:1.5em;">'.$EstimateList['0']['Estimate']['prop_city'].' '.$EstimateList['0']['Estimate']['prop_state'].' '.$EstimateList['0']['Estimate']['prop_address'].'</td>
                            </tr>
                            <tr>
                                <td style="font-size:1.1em;">'.$EstimateList['0']['Estimate']['prop_description'].'</td>
                            </tr>
                        </table>
                        
                        <table style="width: 100%;">
                            <tr>
                                <td style="height:30px;"></td>
                            </tr>
                        </table>';
if($EstimateList['0']['Estimate']['isRentReady'] == '1') {                             
if(isset($EstimateList['0']['Estimate']['rentready']) && !empty($EstimateList['0']['Estimate']['rentready'])){
$rentreadyList  =  json_decode($EstimateList['0']['Estimate']['rentready'],true) ;	
$htmlcontent.='<div class="propert-info-sec"><h3>Property Details:</h3> <table  width="99%" cellpadding="5px" cellspacing="0px" border="0.5" style="border-collapse: collapse; width: 100%;">';
foreach($rentreadyList as $key => $item) { 
   
if($item['type']=='switch' && $item['value']=='1'){
$value='YES';
}elseif($item['type']=='switch' && $item['value']=='0'){
  $value='NO';
}else{
  $value= $item['value'];
}
          
  if(empty( $value)){
   $value = '-';
  }
                 $htmlcontent.='<tr>';	 
             $htmlcontent.='<td>'.$item['title'].'</td>';
             $htmlcontent.='<td>'.$value.'</td>';
                 $htmlcontent.='</tr>';
   }
$htmlcontent.='</table></div>';
   }
}
                        
$htmlcontent.='<div>
                            <table width="100%" cellpadding="5px" cellspacing="0px" border="0.3" style="border-collapse: collapse; width: 100%;"><tr><th style="padding: 5px;vertical-align: middle;font-weight: normal; text-align:left;">Category / Item</th><th style="padding: 5px;vertical-align: middle;font-weight: normal; text-align:left;">Description</th><th style="padding: 5px;vertical-align: middle;font-weight: normal;text-align: center;" >Price</th></tr>';
                            $i=1;

foreach($ItemsList as $data) {
if(isset($data['EstimateItem']['itemimagetotal']['0']['Image']['image_name']) && !empty( $data['EstimateItem']['itemimagetotal']['0']['Image']['image_name']) ) {
$image_name ='item/'.$data['EstimateItem']['itemimagetotal']['0']['Image']['image_name'];
}else{
$image_name ='noimage.jpg';
}
                                $htmlcontent.='<tr>
                                <td colspan="3" style="color: rgb(28, 28, 28);">'.$data['EstimateItem']['category_name'].'</td>
                                </tr>
                                <tr>
                                <td style="padding: 25px 0;vertical-align: middle;line-height: 1.42857;">
                                    <img src="'.APP.'webroot/img/'.$image_name.'" align="left" width="50" height="50">
                                </td>
                                <td style="padding: 25px 2px;vertical-align: middle;line-height: 1.42857;color: rgb(140, 140, 140);">'.$data['EstimateItem']['item_desc'].'</td>
                                <td style="padding: 25px 2px;vertical-align: middle;line-height: 1.42857;text-align: center;font-size:1em;">$'.$data['EstimateItem']['admin_item_cost'].'</td>
                                </tr>';
                                $i++; 
if($i%7==0)
{
$htmlcontent.='</table>
<tcpdf method="AddPage" />
<table width="100%" cellpadding="5px" cellspacing="0px" border="0.3" style="border-collapse: collapse; width: 100%;"><tr><th style="padding: 5px;vertical-align: middle;font-weight: normal; text-align:left;">Category / Item</th><th style="padding: 5px;vertical-align: middle;font-weight: normal; text-align:left;">Description</th><th style="padding: 5px;vertical-align: middle;font-weight: normal;text-align: center;" >Price</th></tr>';
}
}
        $htmlcontent.='<tr>
                            <td colspan="2" style="text-align: right;color: rgb(28, 28, 28);">Total</td>
                            <td style="text-align: center;color: rgb(28, 28, 28);">$ '.$EstimateList['0']['Estimate']['admin_total_cost'].'</td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: right;color: rgb(28, 28, 28);">Service Tax</td>
                            <td style="text-align: center;color: rgb(28, 28, 28);">'.$EstimateList['0']['Estimate']['service_tax'].' %</td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: right;color: rgb(28, 28, 28);">Discount</td>
                            <td style="text-align: center;color: rgb(28, 28, 28);">'.$EstimateList['0']['Estimate']['discount'].' %</td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: right;color: rgb(28, 28, 28);">Grand Total</td>
                            <td style="text-align: center;color: rgb(28, 28, 28);">$ '.$EstimateList['0']['Estimate']['admin_grand_total_cost'].'</td>
                        </tr>';
$htmlcontent.='</table>';
$htmlcontent.=' <table style="width: 100%;">
                            <tr>
                                <td style="height:30px;"></td>
                            </tr>
<tr>
<td style="font-size:1.1em;text-align:left;color: rgb(28, 28, 28);">Comments <br></td>
</tr>
<tr>
   <td style="text-align:left;"><table width="100%" cellpadding="5px" cellspacing="0px" border="0.3" style="border-collapse: collapse; width: 100%;"><tr><td> '.$EstimateList['0']['Estimate']['commentclient'].' </td></tr></table></td></tr></table></div>';
$tcpdf->writeHTML($htmlcontent, true, 0, true, 0);
$file_name	=	$status.'_'.$EstimateList['0']['Estimate']['work_order'].'_'. date("Y-m-d").'.pdf';
$tcpdf->Output($file_name, 'D');
}
#########################################################################	
function admindetails(){
$modelUser    = ClassRegistry::init('User');
$AdminInfo    = $modelUser->find('first',array('conditions'=>array('id'=>1)));	
return $AdminInfo;
}
#####################################################################################


}
